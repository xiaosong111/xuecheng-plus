package com.xuecheng.base.exception;

/**
 * @Author QG
 * @Date 2023/2/16 14:02
 * @description  本项目自定义异常
 **/
public class XuechengPlusException extends RuntimeException{

    private String errMessage;

    public XuechengPlusException() {
        super();
    }

    public XuechengPlusException(String errMessage) {
        super(errMessage);
        this.errMessage = errMessage;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public static void cast(String errMessage) {
        throw new  XuechengPlusException(errMessage);
    }

    public static void cast(CommonError commonError) {
        throw new XuechengPlusException(commonError.getMessage());
    }
}
