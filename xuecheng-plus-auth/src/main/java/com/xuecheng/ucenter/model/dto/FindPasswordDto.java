package com.xuecheng.ucenter.model.dto;

import lombok.Data;

/**
 * @Author QG
 * @Date 2023/4/9 15:08
 * @description
 **/
@Data
public class FindPasswordDto {

    /**
     * 手机号
     */
    private String cellphone;
    /**
     * 邮箱
     */
    private String email;
    //验证码的key
    private String checkcodekey;
    //验证码
    private String checkcode;
    //确认的新密码
    private String confirmpwd;
    //新密码
    private String password;
}
