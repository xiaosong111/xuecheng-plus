package com.xuecheng.ucenter.service;

import com.xuecheng.ucenter.model.dto.FindPasswordDto;

/**
 * @Author QG
 * @Date 2023/4/9 15:12
 * @description
 **/
public interface LoginService {
    /**
     * 找回密码
     * @param findPasswordDto
     * @return
     */
    String findpassword(FindPasswordDto findPasswordDto);
}
