package com.xuecheng.ucenter.service;

import com.xuecheng.ucenter.model.po.XcUser;

import java.util.Map;

/**
 * @Author QG
 * @Date 2023/3/22 15:23
 * @description 微信认证
 **/
public interface WxAuthService {

    XcUser wxAuth(String code);

    public XcUser addWxUser(Map<String, String> userInfo_map);
}
