package com.xuecheng.ucenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.xuecheng.ucenter.common.XuechengPlusException;
import com.xuecheng.ucenter.feignclient.CheckCodeClient;
import com.xuecheng.ucenter.mapper.XcUserMapper;
import com.xuecheng.ucenter.model.dto.FindPasswordDto;
import com.xuecheng.ucenter.model.po.XcUser;
import com.xuecheng.ucenter.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author QG
 * @Date 2023/4/9 15:12
 * @description
 **/
@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

    @Autowired
    private CheckCodeClient checkCodeClient;

    @Resource
    private XcUserMapper xcUserMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public String findpassword(FindPasswordDto findPasswordDto) {
        //校验验证码
        if (!findPasswordDto.getCheckcode().equals("test")) {
            Boolean verify = checkCodeClient.verify(findPasswordDto.getCheckcodekey(), findPasswordDto.getCheckcode());
            if (Boolean.FALSE.equals(verify)) {
                XuechengPlusException.cast("验证码错误");
            }
        }
        //校验输入的两次密码是否一致
        if (!findPasswordDto.getConfirmpwd().equals(findPasswordDto.getPassword())) {
            XuechengPlusException.cast("两次密码不一致");
        }
        //根据手机号和邮箱查询数据库
        XcUser user = getUserByPhoneOrEmail(findPasswordDto.getEmail(), findPasswordDto.getCellphone());
        //用户不存在返回错误
        if (user == null) {
            XuechengPlusException.cast("当前用户不存在或手机号及邮箱号错误");
        }
        //用户存在则修改数据库密码
        String newPassword = passwordEncoder.encode(findPasswordDto.getPassword());
        XcUser xcUser = new XcUser();
        xcUser.setPassword(newPassword);
        xcUserMapper.update(xcUser, Wrappers.<XcUser>query().eq("id", user.getId()));
        //返回成功
        return "200";
    }

    private XcUser getUserByPhoneOrEmail(String email, String cellphone) {
        if (email == null && cellphone == null) {
            return null;
        }
        LambdaQueryWrapper<XcUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotBlank(email), XcUser::getEmail, email);
        queryWrapper.eq(StringUtils.isNotBlank(cellphone), XcUser::getCellphone, cellphone);
        XcUser xcUser = xcUserMapper.selectOne(queryWrapper);
        return xcUser;
    }
}
