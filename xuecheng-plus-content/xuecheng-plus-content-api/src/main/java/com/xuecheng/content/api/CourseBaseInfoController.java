package com.xuecheng.content.api;

import com.xuecheng.base.model.PageParams;
import com.xuecheng.base.model.PageResult;
import com.xuecheng.content.model.dto.AddCourseDto;
import com.xuecheng.content.model.dto.CourseBaseInfoDto;
import com.xuecheng.content.model.dto.ModifyCourseDto;
import com.xuecheng.content.model.dto.QueryCourseParamsDto;
import com.xuecheng.content.model.po.CourseBase;
import com.xuecheng.content.service.CourseBaseInfoService;
import com.xuecheng.content.util.SecurityUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * @Author QG
 * @Date 2023/2/15 10:40
 * @description 课程信息编辑接口
 **/
@Api(value = "课程信息编辑接口",tags = "课程信息编辑接口")
@RestController
public class CourseBaseInfoController {

    @Autowired
    private CourseBaseInfoService courseBaseInfoService;

    @ApiOperation(value = "课程信息分页查询",tags = "课程信息分页查询")
    @PostMapping("/course/list")
    @PreAuthorize("hasAuthority('xc_teachmanager_course_list')")
    public PageResult<CourseBase> list(PageParams pageParams, @RequestBody QueryCourseParamsDto queryCourseParams) {
        return courseBaseInfoService.queryCourseBaseList(pageParams,queryCourseParams);
    }

    @ApiOperation("新增课程基础信息")
    @PostMapping("/course")
    public CourseBaseInfoDto createCourseBase(@RequestBody @Validated AddCourseDto addCourseDto){
        //todo 这里companyId应该换成查询当前用户所在公司id
        return courseBaseInfoService.createCourseBase(1L,addCourseDto);
    }

    @ApiOperation("查询课程信息")
    @GetMapping("/course/{courseId}")
    public CourseBaseInfoDto getCourseById(@PathVariable("courseId") Long courseId) {
//        SecurityUtil.XcUser user = SecurityUtil.getUser();
//        System.out.println(user);
        return courseBaseInfoService.getCourseBaseInfo(courseId);
    }

    @ApiOperation("修改课程信息")
    @PutMapping("/course")
    public CourseBaseInfoDto modifyCourseBase(@RequestBody ModifyCourseDto modifyCourseDto) {
        //处理一个参数，oss的pic的路径与minio不同，前端为minio的路径
//        String pic = modifyCourseDto.getPic();
//        pic = pic.substring(pic.indexOf("/",1));
//        modifyCourseDto.setPic(pic);
        return courseBaseInfoService.modifyCourseBase(1L ,modifyCourseDto);
    }

    @ApiOperation("删除课程及其相关信息")
    @DeleteMapping("/course/{courseId}")
    public void deleteCourse(@PathVariable String courseId) {
        courseBaseInfoService.deleteCourse(courseId);
    }

}
