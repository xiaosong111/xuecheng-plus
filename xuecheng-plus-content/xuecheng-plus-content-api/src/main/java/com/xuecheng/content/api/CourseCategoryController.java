package com.xuecheng.content.api;

import com.xuecheng.content.model.dto.CourseCategoryTreeDto;
import com.xuecheng.content.service.CourseCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author QG
 * @Date 2023/2/16 9:48
 * @description
 **/
@Api(value = "课程分类信息相关",tags = "课程分类信息相关")
@RestController
public class CourseCategoryController {

    private static final  String ROOT_VALUE = "1";

    @Autowired
    private CourseCategoryService courseCategoryService;

    @ApiOperation(value = "查询课程分类信息",tags = "返回树形结构信息")
    @GetMapping("/course-category/tree-nodes")
    public List<CourseCategoryTreeDto> queryTreeNodes() {
        return courseCategoryService.queryTreeNodes(ROOT_VALUE);
    }
}
