package com.xuecheng.content.api;

import com.xuecheng.base.model.RestResponse;
import com.xuecheng.content.model.dto.BindTeachplanMediaDto;
import com.xuecheng.content.model.dto.SaveTeachplanDto;
import com.xuecheng.content.model.dto.TeachplanDto;
import com.xuecheng.content.service.TeachPlanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author QG
 * @Date 2023/2/17 9:30
 * @description 课程计划控制器
 **/
@Api(value = "课程计划相关接口",tags = "课程计划相关接口")
@RestController
public class TeachPlanController {

    @Autowired
    private TeachPlanService teachPlanService;

    @ApiOperation("根据课程id获取课程的课程计划")
    @GetMapping("/teachplan/{courseId}/tree-nodes")
    public List<TeachplanDto> selectTreeNodes(@PathVariable("courseId") Long courseId) {
        return teachPlanService.selectTrrNodes(courseId);
    }

    @ApiOperation("课程计划创建或修改")
    @PostMapping("/teachplan")
    public void saveTeachPlan(@RequestBody SaveTeachplanDto saveTeachplanDto) {
        teachPlanService.saveTeachPlan(saveTeachplanDto);
    }

    @ApiOperation("根据id删除课程计划")
    @DeleteMapping("/teachplan/{teachplanId}")
    public void deleteTeachPlan(@PathVariable("teachplanId") Long teachplanId) {
        teachPlanService.deleteTeachPlan(teachplanId);
    }

    @ApiOperation("移动课程计划")
    @PostMapping("/teachplan/{moveType}/{teachId}")
    public void moveTeachPlan(@PathVariable String moveType, @PathVariable Long teachId) {
        teachPlanService.moveTeachPlan(moveType,teachId);
    }

    @ApiOperation(value = "课程计划和媒资文件绑定")
    @PostMapping("/teachplan/association/media")
    public void associationMedia(@RequestBody BindTeachplanMediaDto bindTeachplanMediaDto) {
        teachPlanService.associationMedia(bindTeachplanMediaDto);
    }

    @ApiOperation("删除课程计划和媒资文件的绑定关系")
    @DeleteMapping("/teachplan/association/media/{teachPlanId}/{mediaId}")
    public RestResponse deleteBindByTeachPlanMedia(@PathVariable("mediaId") String mediaId, @PathVariable("teachPlanId") String teachPlanId) {
        return teachPlanService.deleteBindByTeachPlanMedia(teachPlanId,mediaId);
    }

}
