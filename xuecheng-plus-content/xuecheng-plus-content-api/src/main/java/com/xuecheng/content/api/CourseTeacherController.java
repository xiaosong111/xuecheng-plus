package com.xuecheng.content.api;

import com.xuecheng.content.model.po.CourseTeacher;
import com.xuecheng.content.service.CourseTeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author QG
 * @Date 2023/2/19 11:51
 * @description 课程老师控制器
 **/
@Api(value = "课程老师信息相关",tags = "课程老师信息相关")
@RestController
public class CourseTeacherController {

    @Autowired
    private CourseTeacherService courseTeacherService;

    @ApiOperation("根据课程id查询课程老师信息")
    @GetMapping("/courseTeacher/list/{courseId}")
    public List<CourseTeacher> queryCourseTeacher(@PathVariable Long courseId) {
        return courseTeacherService.queryCourseTeacher(courseId);
    }

    @ApiOperation(("添加或修改课程老师请求"))
    @PostMapping("/courseTeacher")
    public CourseTeacher saveCourseTeacher(@RequestBody CourseTeacher courseTeacher) {
        return courseTeacherService.saveOrUpdateCourseTeacher(courseTeacher);
    }

    @ApiOperation("删除教师信息")
    @DeleteMapping("/courseTeacher/course/{courseId}/{teacherId}")
    public void deleteCourseTeacher(@PathVariable String courseId, @PathVariable String teacherId) {
        courseTeacherService.deleteCourseTeacher(courseId,teacherId);
    }

}
