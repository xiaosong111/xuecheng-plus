package com.xuecheng.content.model.dto;

import com.xuecheng.content.model.po.CourseCategory;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author QG
 * @Date 2023/2/16 9:50
 * @description 课程分类树型结点dto
 **/
@Data
public class CourseCategoryTreeDto extends CourseCategory implements Serializable {

    /**
     * 子节点集合
     */
    private List<CourseCategoryTreeDto> childrenTreeNodes;
}
