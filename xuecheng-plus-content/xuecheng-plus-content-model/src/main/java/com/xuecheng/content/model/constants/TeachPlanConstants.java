package com.xuecheng.content.model.constants;

import lombok.Data;

/**
 * @Author QG
 * @Date 2023/2/19 10:14
 * @description 课程计划相关常量
 **/
@Data
public class TeachPlanConstants {

    /**
     * 向下移动
     */
    public static final String MOVE_DOWN = "movedown";

    /**
     * 向下移动
     */
    public static final String MOVE_UP = "moveup";

    /**
     * 向上移动标识
     */
    public static final Integer UP = 0;

    /**
     * 向下移动标识
     */
    public static final Integer DOWN = 1;
    /**
     * 课程未发布
     */
    public static final String COURSE_UN_PUBLISH = "1";
}
