package com.xuecheng.content.model.dto;

import lombok.Data;

/**
 * @Author QG
 * @Date 2023/2/16 17:11
 * @description 修改课程信息dto
 **/
@Data
public class ModifyCourseDto extends AddCourseDto{
    /**
     * 课程id
     */
    private Long id;
}
