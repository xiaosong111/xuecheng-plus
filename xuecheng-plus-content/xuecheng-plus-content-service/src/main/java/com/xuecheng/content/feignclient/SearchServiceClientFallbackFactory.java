package com.xuecheng.content.feignclient;

import com.xuecheng.content.feignclient.po.CourseIndex;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Author QG
 * @Date 2023/3/15 21:33
 * @description
 **/
@Slf4j
@Component
public class SearchServiceClientFallbackFactory implements FallbackFactory {
    @Override
    public Object create(Throwable throwable) {
        return new SearchServiceClient() {
            @Override
            public Boolean add(CourseIndex courseIndex) {
                log.debug("搜索服务熔断，触发降级，异常：{}",throwable.toString(),throwable);
                return null;
            }
        };
    }
}
