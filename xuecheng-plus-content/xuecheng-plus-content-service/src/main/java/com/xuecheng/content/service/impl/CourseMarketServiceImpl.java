package com.xuecheng.content.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xuecheng.content.service.CourseMarketService;
import com.xuecheng.content.mapper.CourseMarketMapper;
import com.xuecheng.content.model.po.CourseMarket;
import org.springframework.stereotype.Service;

/**
 * @Author QG
 * @Date 2023/2/16 20:19
 * @description
 **/
@Service
public class CourseMarketServiceImpl extends ServiceImpl<CourseMarketMapper,CourseMarket> implements CourseMarketService {
}
