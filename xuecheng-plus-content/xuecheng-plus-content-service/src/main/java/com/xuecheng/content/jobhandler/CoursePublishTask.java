package com.xuecheng.content.jobhandler;

import com.xuecheng.base.exception.XuechengPlusException;
import com.xuecheng.content.feignclient.SearchServiceClient;
import com.xuecheng.content.feignclient.po.CourseIndex;
import com.xuecheng.content.mapper.CoursePublishMapper;
import com.xuecheng.content.model.po.CoursePublish;
import com.xuecheng.content.service.CoursePublishService;
import com.xuecheng.messagesdk.model.po.MqMessage;
import com.xuecheng.messagesdk.service.MessageProcessAbstract;
import com.xuecheng.messagesdk.service.MqMessageService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.File;

/**
 * @Author QG
 * @Date 2023/3/15 10:39
 * @description 课程发布任务
 **/
@Slf4j
@Component
public class CoursePublishTask extends MessageProcessAbstract {

    @Autowired
    private CoursePublishService coursePublishService;
    @Autowired
    private SearchServiceClient searchServiceClient;
    @Resource
    private CoursePublishMapper coursePublishMapper;


    //课程发布消息类型
    public static final String MESSAGE_TYPE = "course_publish";
    //任务调度入口
    @XxlJob("CoursePublishJobHandler")
    public void coursePublishJobHandler() throws Exception {
        // 分片参数
        int shardIndex = XxlJobHelper.getShardIndex();
        int shardTotal = XxlJobHelper.getShardTotal();
        log.debug("shardIndex="+shardIndex+",shardTotal="+shardTotal);
        //参数:分片序号、分片总数、消息类型、一次最多取到的任务数量、一次任务调度执行的超时时间
        process(shardIndex,shardTotal,MESSAGE_TYPE,5,60);
    }


    /**
     * 具体的处理方法
     * @param mqMessage 执行任务内容
     * @return
     */
    @Override
    public boolean execute(MqMessage mqMessage) {
        //获取消息相关的业务信息
        String businessKey1 = mqMessage.getBusinessKey1();
        long courseId = Integer.parseInt(businessKey1);
        //生成html文件，并上传到Minio
        generateCourseHtml(mqMessage,courseId);

        //创建课程信息缓存到redis
        saveCourseCache(mqMessage,courseId);
        //生成课程索引并存储到es
        saveCourseIndex(mqMessage,courseId);

        //删除消息表中信息并将信息存储到消息历史表中  这部分操作在process方法中实现了
        return true;
    }

    private void generateCourseHtml(MqMessage mqMessage, long courseId) {
        log.debug("开始进行课程静态化,课程id:{}",courseId);
        //消息id
        Long id = mqMessage.getId();
        //消息处理的service
        MqMessageService mqMessageService = this.getMqMessageService();
        //处理消息幂等性
        if(mqMessageService.getStageOne(id) == 1L) {
            log.debug("课程静态化已处理直接返回，课程id:{}",courseId);
            return;
        }
        //生成并上传html文件
        File file = coursePublishService.generateCourseHtml(courseId);
        coursePublishService.uploadCourseHtml(courseId,file);
        //修改第一阶段状态
        mqMessageService.completedStageOne(id);
    }

    private void saveCourseCache(MqMessage mqMessage, long courseId) {

    }

    //保存课程索引信息
    public void saveCourseIndex(MqMessage mqMessage,long courseId){
        log.debug("保存课程索引信息,课程id:{}",courseId);
        //消息id
        Long id = mqMessage.getId();
        //消息处理的service
        MqMessageService mqMessageService = this.getMqMessageService();
        //消息幂等性处理
        int stageTwo = mqMessageService.getStageTwo(id);
        if(stageTwo == 2){
            log.debug("课程索引已处理直接返回，课程id:{}",courseId);
            return ;
        }
        Boolean result = coursePublishService.saveCourseIndex(courseId);
        if(result){
            //保存第二阶段状态
            mqMessageService.completedStageTwo(id);
        }

    }


}
