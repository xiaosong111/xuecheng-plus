package com.xuecheng.content.mapper;

import com.xuecheng.content.model.po.CoursePublish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13302
* @description 针对表【course_publish(课程发布)】的数据库操作Mapper
* @createDate 2023-03-12 09:54:31
* @Entity com.xuecheng.content.po.CoursePublish
*/
public interface CoursePublishMapper extends BaseMapper<CoursePublish> {

}




