package com.xuecheng.content.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.xuecheng.content.mapper.CourseTeacherMapper;
import com.xuecheng.content.service.CourseTeacherService;
import com.xuecheng.base.exception.CommonError;
import com.xuecheng.base.exception.XuechengPlusException;
import com.xuecheng.content.model.po.CourseTeacher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author QG
 * @Date 2023/2/19 11:59
 * @description 课程老师相关服务
 **/
@Slf4j
@Service
public class CourseTeacherServiceImpl implements CourseTeacherService {

    @Resource
    private CourseTeacherMapper courseTeacherMapper;

    @Override
    public List<CourseTeacher> queryCourseTeacher(Long courseId) {
        List<CourseTeacher> courseTeachers = courseTeacherMapper.selectList(Wrappers.<CourseTeacher>lambdaQuery().eq(CourseTeacher::getCourseId, courseId));
        return courseTeachers;
    }

    @Override
    public CourseTeacher saveOrUpdateCourseTeacher(CourseTeacher courseTeacher) {
        if(courseTeacher == null) {
            XuechengPlusException.cast(CommonError.PARAMS_ERROR);
        }
        if(courseTeacher.getId() == null) {
            //id为空则新增
            courseTeacherMapper.insert(courseTeacher);
            return courseTeacher;
        }
        //id不为空则修改
        courseTeacherMapper.updateById(courseTeacher);
        return courseTeacher;
    }

    @Override
    public void deleteCourseTeacher(String courseId, String teacherId) {
        if(courseId == null || teacherId == null) {
            XuechengPlusException.cast(CommonError.PARAMS_ERROR);
        }
        courseTeacherMapper.delete(Wrappers.<CourseTeacher>lambdaQuery().eq(CourseTeacher::getCourseId,courseId).eq(CourseTeacher::getId,teacherId));
    }
}
