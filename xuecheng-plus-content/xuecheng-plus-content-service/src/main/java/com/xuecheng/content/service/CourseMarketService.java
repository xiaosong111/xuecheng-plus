package com.xuecheng.content.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.content.model.po.CourseMarket;

/**
 * @Author QG
 * @Date 2023/2/16 20:19
 * @description
 **/
public interface CourseMarketService extends IService<CourseMarket> {
}
