package com.xuecheng.content.service.impl;

import com.xuecheng.content.service.CourseCategoryService;
import com.xuecheng.content.mapper.CourseCategoryMapper;
import com.xuecheng.content.model.dto.CourseCategoryTreeDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author QG
 * @Date 2023/2/16 9:47
 * @description
 **/
@Slf4j
@Service
public class CourseCategoryServiceImpl implements CourseCategoryService {

    @Resource
    private CourseCategoryMapper courseCategoryMapper;

    /**
     * 查询课程分类，并将数据组织为树形结构
     * @param id 根节点id
     * @return
     */
    @Override
    public List<CourseCategoryTreeDto> queryTreeNodes(String id) {
        //获取所有数据
        List<CourseCategoryTreeDto> courseCategoryTreeDtos = courseCategoryMapper.getAllExRoot(id);
        //构建树结构
        List<CourseCategoryTreeDto> resTree = courseCategoryTreeDtos.stream()
                .filter(m -> m.getParentid().equals(id))
                .map(item -> {
                    item.setChildrenTreeNodes(getChildrenNodes(item, courseCategoryTreeDtos));
                    return item;
                }).collect(Collectors.toList());

        return resTree;
    }

    /**
     * 获取current节点的子节点集合
     * @param current
     * @param menus
     * @return
     */
    public List<CourseCategoryTreeDto> getChildrenNodes(CourseCategoryTreeDto current,List<CourseCategoryTreeDto> menus) {
        List<CourseCategoryTreeDto> collect = menus.stream()
                .filter(m -> m.getParentid().equals(current.getId())).collect(Collectors.toList());
//                .map(item -> {
//                    item.setChildrenTreeNodes(getChildrenNodes(item, menus));
//                    return item;
//                }).collect(Collectors.toList());
        return collect;
    }
}
