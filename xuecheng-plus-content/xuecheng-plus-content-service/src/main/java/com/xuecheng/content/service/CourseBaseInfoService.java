package com.xuecheng.content.service;

import com.xuecheng.base.model.PageParams;
import com.xuecheng.base.model.PageResult;
import com.xuecheng.content.model.dto.AddCourseDto;
import com.xuecheng.content.model.dto.CourseBaseInfoDto;
import com.xuecheng.content.model.dto.ModifyCourseDto;
import com.xuecheng.content.model.dto.QueryCourseParamsDto;
import com.xuecheng.content.model.po.CourseBase;

/**
 * @Author QG
 * @Date 2023/2/15 14:38
 * @description
 **/
public interface CourseBaseInfoService {
    /**
     * 分页查询课程信息数据
     * @param pageParams
     * @param queryCourseParams
     * @return
     */
    PageResult<CourseBase> queryCourseBaseList(PageParams pageParams, QueryCourseParamsDto queryCourseParams);

    /**
     * 课程基本信息增加
     * @param companyId  机构id
     * @param addCourseDto
     * @return 课程的基本信息，包括课程信息和课程营销信息
     */
    CourseBaseInfoDto createCourseBase(Long companyId, AddCourseDto addCourseDto);


    /**
     * 查询课程基本信息
     * @param userId
     * @return
     */
    CourseBaseInfoDto getCourseBaseInfo(Long courseId);

    /**
     * 修改课程信息
     * @param courseId
     * @param modifyCourseDto
     * @return
     */
    CourseBaseInfoDto modifyCourseBase(Long companyId, ModifyCourseDto modifyCourseDto);

    /**
     * 删除课程相关的基本信息，营销信息，课程计划，课程教师信息
     * @param courseId
     */
    void deleteCourse(String courseId);
}
