package com.xuecheng.content.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xuecheng.content.mapper.*;
import com.xuecheng.content.service.CourseBaseInfoService;
import com.xuecheng.content.service.CourseMarketService;
import com.xuecheng.base.exception.XuechengPlusException;
import com.xuecheng.base.model.PageParams;
import com.xuecheng.base.model.PageResult;
import com.xuecheng.content.model.constants.TeachPlanConstants;
import com.xuecheng.content.model.dto.AddCourseDto;
import com.xuecheng.content.model.dto.CourseBaseInfoDto;
import com.xuecheng.content.model.dto.ModifyCourseDto;
import com.xuecheng.content.model.dto.QueryCourseParamsDto;
import com.xuecheng.content.model.po.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author QG
 * @Date 2023/2/15 14:41
 * @description
 **/
@Service
public class CourseBaseInfoServiceImpl implements CourseBaseInfoService {

    @Resource
    private CourseBaseMapper courseBaseMapper;

    @Resource
    private CourseMarketMapper courseMarketMapper;

    @Resource
    private CourseCategoryMapper courseCategoryMapper;

    @Resource
    private CourseMarketService courseMarketService;

    @Resource
    private TeachplanMapper teachplanMapper;
    @Resource
    private TeachplanMediaMapper teachplanMediaMapper;
    @Resource
    private CourseTeacherMapper courseTeacherMapper;

    @Override
    public PageResult<CourseBase> queryCourseBaseList(PageParams pageParams, QueryCourseParamsDto queryCourseParams) {
        //封装查询条件
        Page page = new Page(pageParams.getPageNo(), pageParams.getPageSize());
        LambdaQueryWrapper<CourseBase> queryWrapper = new LambdaQueryWrapper<>();
        //构建查询条件，根据课程名称查询
        queryWrapper.like(StringUtils.isNotEmpty(queryCourseParams.getCourseName()),CourseBase::getName,queryCourseParams.getCourseName());
        //构建查询条件，根据课程审核状态查询
        queryWrapper.eq(StringUtils.isNotEmpty(queryCourseParams.getAuditStatus()),CourseBase::getAuditStatus,queryCourseParams.getAuditStatus());
        //构建查询条件，根据课程发布状态查询
        queryWrapper.eq(StringUtils.isNotEmpty(queryCourseParams.getPublishStatus()),CourseBase::getStatus,queryCourseParams.getPublishStatus());
        Page pageResult = courseBaseMapper.selectPage(page, queryWrapper);
        //封装结果
        List records = pageResult.getRecords();
        long total = pageResult.getTotal();
        return new PageResult<CourseBase>(records,total,pageParams.getPageNo(),pageParams.getPageSize());
    }

    @Transactional
    @Override
    public CourseBaseInfoDto createCourseBase(Long companyId,AddCourseDto dto) {

        //合法性校验
        //收费课程必须写价格且价格大于0
        if(dto.getCharge().equals("201001")){
            Float price = dto.getPrice();
            if(price == null || price.floatValue()<=0){
                throw new XuechengPlusException("课程设置了收费价格不能为空且必须大于0");
            }
        }
        //新增对象
        CourseBase courseBaseNew = new CourseBase();
        //将填写的课程信息赋值给新增对象
        BeanUtils.copyProperties(dto,courseBaseNew);
        //设置审核状态
        courseBaseNew.setAuditStatus("202002");
        //设置发布状态
        courseBaseNew.setStatus("203001");
        //机构id
        courseBaseNew.setCompanyId(companyId);
        //添加时间
        courseBaseNew.setCreateDate(LocalDateTime.now());
        //插入课程基本信息表
        int insert = courseBaseMapper.insert(courseBaseNew);
        Long courseId = courseBaseNew.getId();
        //课程营销信息
        CourseMarket courseMarketNew = new CourseMarket();
        BeanUtils.copyProperties(dto,courseMarketNew);
        courseMarketNew.setId(courseId);
        //收费规则
        String charge = dto.getCharge();

        //插入课程营销信息
        int insert1 = courseMarketMapper.insert(courseMarketNew);

        if(insert<=0 || insert1<=0){
            throw new RuntimeException("新增课程基本信息失败");
        }
        //添加成功
        //返回添加的课程信息
        return getCourseBaseInfo(courseId);

    }
    //根据课程id查询课程基本信息，包括基本信息和营销信息
    public CourseBaseInfoDto getCourseBaseInfo(Long courseId){

        CourseBase courseBase = courseBaseMapper.selectById(courseId);
        CourseMarket courseMarket = courseMarketMapper.selectById(courseId);

        if(courseBase == null){
            return null;
        }
        CourseBaseInfoDto courseBaseInfoDto = new CourseBaseInfoDto();
        BeanUtils.copyProperties(courseBase,courseBaseInfoDto);
        if(courseMarket != null){
            BeanUtils.copyProperties(courseMarket,courseBaseInfoDto);
        }

        //查询分类名称
        CourseCategory courseCategoryBySt = courseCategoryMapper.selectById(courseBase.getSt());
        courseBaseInfoDto.setStName(courseCategoryBySt.getName());
        CourseCategory courseCategoryByMt = courseCategoryMapper.selectById(courseBase.getMt());
        courseBaseInfoDto.setMtName(courseCategoryByMt.getName());
        return courseBaseInfoDto;
    }

    @Transactional
    @Override
    public CourseBaseInfoDto modifyCourseBase(Long companyId, ModifyCourseDto modifyCourseDto) {
        //校验参数
        CourseBase courseBase = courseBaseMapper.selectById(modifyCourseDto.getId());
        if(courseBase == null) {
            XuechengPlusException.cast("该课程不存在");
        }
        if(!courseBase.getCompanyId().equals(companyId)) {
            XuechengPlusException.cast("本机构只能修改本机构的信息");
        }
        if(modifyCourseDto.getCharge().equals("201001")){
            Float price = modifyCourseDto.getPrice();
            if(price == null || price.floatValue()<=0){
                throw new XuechengPlusException("课程设置了收费价格不能为空且必须大于0");
            }
        }
        //封装并保存数据
        CourseBase courseBaseInfo = new CourseBase();
        BeanUtils.copyProperties(modifyCourseDto,courseBaseInfo);
        courseBaseMapper.updateById(courseBaseInfo);
        CourseMarket courseMarket = new CourseMarket();
        BeanUtils.copyProperties(modifyCourseDto,courseMarket);
        courseMarket.setId(courseBaseInfo.getId());
        //存在则修改，不存在则保存
        courseMarketService.saveOrUpdate(courseMarket);
        //返回结果
        return getCourseBaseInfo(courseBaseInfo.getId());
    }

    @Override
    @Transactional
    public void deleteCourse(String courseId) {
        CourseBase courseBase = courseBaseMapper.selectById(courseId);
        if((courseBase.getStatus().equals(TeachPlanConstants.COURSE_UN_PUBLISH))) {
            XuechengPlusException.cast("课程尚未下架！");
        }
        //删除课程基本信息
        courseBaseMapper.deleteById(courseId);
        //删除课程营销信息
        courseMarketMapper.deleteById(courseId);
        //删除课程计划，及其媒资信息
        teachplanMapper.delete(Wrappers.<Teachplan>lambdaQuery().eq(Teachplan::getCourseId,courseId));
        teachplanMediaMapper.delete(Wrappers.<TeachplanMedia>lambdaQuery().eq(TeachplanMedia::getCourseId,courseId));
        //删除教师信息
        courseTeacherMapper.delete(Wrappers.<CourseTeacher>lambdaQuery().eq(CourseTeacher::getCourseId,courseId));
    }
}
