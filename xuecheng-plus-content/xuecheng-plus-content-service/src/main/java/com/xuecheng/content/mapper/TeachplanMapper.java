package com.xuecheng.content.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xuecheng.content.model.dto.TeachplanDto;
import com.xuecheng.content.model.po.Teachplan;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 课程计划 Mapper 接口
 * </p>
 *
 * @author itcast
 */
public interface TeachplanMapper extends BaseMapper<Teachplan> {

    /**
     * 根据课程id查询课程的课程计划
     */
    List<TeachplanDto> selectTreeNodes(Long courseId);

    /**
     * 根据课程id和父节点id查询所有order
     * @param courseId
     * @param parentId
     * @return
     */
    List<Integer> selectMaxOrder(@Param("courseId") Long courseId,@Param("parentId") Long parentId);

    /**
     * 根据moveFlag查询课程计划排序前后的同级课程计划集合
     *
     * @param parentid
     * @param orderby
     * @param courseId
     * @param moveFlag 为0向上查，为1向下查
     */
    List<Teachplan> selectMoveList(@Param("parentid") Long parentid,@Param("orderby") Integer orderby
            ,@Param("courseId") Long courseId,@Param("moveFlag") int moveFlag);
}
