package com.xuecheng.content.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.xuecheng.content.config.MultipartSupportConfig;
import com.xuecheng.content.feignclient.MediaServiceClient;
import com.xuecheng.content.feignclient.SearchServiceClient;
import com.xuecheng.content.feignclient.po.CourseIndex;
import com.xuecheng.content.mapper.CourseBaseMapper;
import com.xuecheng.content.mapper.CourseMarketMapper;
import com.xuecheng.content.mapper.CoursePublishMapper;
import com.xuecheng.content.mapper.CoursePublishPreMapper;
import com.xuecheng.content.service.CourseBaseInfoService;
import com.xuecheng.content.service.CoursePublishService;
import com.xuecheng.content.service.CourseTeacherService;
import com.xuecheng.content.service.TeachPlanService;
import com.xuecheng.base.exception.CommonError;
import com.xuecheng.base.exception.XuechengPlusException;
import com.xuecheng.content.model.dto.CourseBaseInfoDto;
import com.xuecheng.content.model.dto.CoursePreviewDto;
import com.xuecheng.content.model.dto.TeachplanDto;
import com.xuecheng.content.model.po.*;
import com.xuecheng.messagesdk.model.po.MqMessage;
import com.xuecheng.messagesdk.service.MqMessageService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author QG
 * @Date 2023/3/9 19:00
 * @description
 **/
@Slf4j
@Service
public class CoursePublishServiceImpl implements CoursePublishService {

    @Autowired
    private CourseBaseInfoService courseBaseInfoService;
    @Autowired
    private TeachPlanService teachPlanService;
    @Resource
    private CourseMarketMapper courseMarketMapper;
    @Autowired
    private CourseTeacherService courseTeacherService;
    @Resource
    private CoursePublishPreMapper coursePublishPreMapper;
    @Resource
    private CourseBaseMapper courseBaseMapper;
    @Resource
    private CoursePublishMapper coursePublishMapper;
    @Autowired
    private MqMessageService mqMessageService;
    @Autowired
    private MediaServiceClient mediaServiceClient;
    @Autowired
    private SearchServiceClient searchServiceClient;


    @Override
    public CoursePreviewDto getCoursePreviewInfo(Long courseId) {
        CoursePreviewDto coursePreviewDto = new CoursePreviewDto();
        //查询课程基本信息和营销信息
        CourseBaseInfoDto courseBaseInfo = courseBaseInfoService.getCourseBaseInfo(courseId);
        coursePreviewDto.setCourseBase(courseBaseInfo);
        //查询课程计划
        List<TeachplanDto> teachplanDtos = teachPlanService.selectTrrNodes(courseId);
        coursePreviewDto.setTeachplans(teachplanDtos);
        return coursePreviewDto;
    }

    @Transactional
    @Override
    public void commitAudit(Long companyId, Long courseId) {
        CourseBaseInfoDto courseBaseInfo = courseBaseInfoService.getCourseBaseInfo(courseId);
        if(!courseBaseInfo.getCompanyId().equals(companyId)) {
            XuechengPlusException.cast("本机构只能提交本机构的课程");
        }
        //当前审核状态为已提交不允许再次提交
        if("202003".equals(courseBaseInfo.getAuditStatus())) {
            XuechengPlusException.cast("当前未为等待审核状态，请等待审核通过后再次提交");
        }
        //课程图片是否填写
        if(StringUtils.isEmpty(courseBaseInfo.getPic())){
            XuechengPlusException.cast("提交失败，请上传课程图片");
        }
        //查询课程相关信息，复制到课程预发布表
        CoursePublishPre coursePublishPre = new CoursePublishPre();
        BeanUtils.copyProperties(courseBaseInfo,coursePublishPre);
        //查询课程营销信息
        CourseMarket courseMarket = courseMarketMapper.selectById(courseId);
        coursePublishPre.setMarket(JSONObject.toJSONString(courseMarket));
        //查询课程计划信息
        List<TeachplanDto> teachplanDtos = teachPlanService.selectTrrNodes(courseId);
        if(teachplanDtos == null || teachplanDtos.size() == 0) {
            XuechengPlusException.cast("课程计划为空，不能提交");
        }
        coursePublishPre.setTeachplan(JSONObject.toJSONString(teachplanDtos));
        //查询教师信息
        List<CourseTeacher> courseTeacherList = courseTeacherService.queryCourseTeacher(courseId);
        coursePublishPre.setTeachers(JSONObject.toJSONString(courseTeacherList));
        //设置预发布记录状态,已提交
        coursePublishPre.setStatus("202003");
        //教学机构id
        coursePublishPre.setCompanyId(companyId);
        //提交时间
        coursePublishPre.setCreateDate(LocalDateTime.now());
        //插入信息到课程预发布表
        CoursePublishPre coursePublishPre1 = coursePublishPreMapper.selectById(courseId);
        if(coursePublishPre1 == null) {
            coursePublishPreMapper.insert(coursePublishPre);
        } else {
            coursePublishPreMapper.updateById(coursePublishPre);
        }
        //更改课程基本信息表状态
        CourseBase courseBase = new CourseBase();
        courseBase.setAuditStatus("202003");
        courseBaseMapper.update(courseBase, Wrappers.<CourseBase>query().eq("id",courseId));
    }

    @Transactional
    @Override
    public void coursepublish(Long companyId, Long courseId) {
        CoursePublishPre coursePublishPre = coursePublishPreMapper.selectById(courseId);
        if(coursePublishPre.getCompanyId() != companyId) {
            XuechengPlusException.cast("本机构只能发布本机构的课程");
        }
        //将预发布表中的记录复制到发布表
        CoursePublish coursePublish = new CoursePublish();
        BeanUtils.copyProperties(coursePublishPre,coursePublish);
        CoursePublish coursePublish1 = coursePublishMapper.selectById(courseId);
        if(coursePublish1 == null) {
            coursePublishMapper.insert(coursePublish);
        } else {
            coursePublishMapper.updateById(coursePublish);
        }
        //向消息表中插入数据
        saveCoursePublishMessage(courseId);

        //删除预发布表中的数据
        coursePublishPreMapper.deleteById(courseId);
    }

    /**
     * 生成静态化html
     * @param courseId 课程id
     * @return
     */
    @Override
    public File generateCourseHtml(Long courseId) {
        File file = null;
        FileOutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            //配置freemarker
            Configuration configuration = new Configuration(Configuration.getVersion());
            //加载模板
            //选指定模板路径,classpath下templates下
            //得到classpath路径
            String classpath = this.getClass().getResource("/").getPath();
            configuration.setDirectoryForTemplateLoading(new File(classpath + "/templates/"));
            //设置字符编码
            configuration.setDefaultEncoding("utf-8");
            //指定模板文件名称
            Template template = configuration.getTemplate("course_template.ftl");
            //获取模型数据
            CoursePreviewDto coursePreviewInfo = this.getCoursePreviewInfo(courseId);
            Map<String, CoursePreviewDto> map = new HashMap<>();
            map.put("model",coursePreviewInfo);
            //静态化
            String content = FreeMarkerTemplateUtils.processTemplateIntoString(template, map);
            inputStream = IOUtils.toInputStream(content);
            //生成html文件
            file = File.createTempFile("course",".html");
            //读写流
            outputStream = new FileOutputStream(file);
            IOUtils.copy(inputStream, outputStream);

        } catch (Exception e) {
            log.debug("异常信息：{}，{}",e.getMessage(),e.toString());
            XuechengPlusException.cast("生成静态html文件异常");
        } finally {
            //关闭文件流
            if(outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                }
            }
            if(inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
            //删除临时文件，避免文件占用空间
            if(file != null) {
                file.delete();
            }
        }
        return file;
    }

    @Override
    public void uploadCourseHtml(Long courseId, File file) {
        try {
            MultipartFile multipartFile = MultipartSupportConfig.getMultipartFile(file);
            String res = mediaServiceClient.uploadFile(multipartFile, "course", courseId + ".html");
            if(res == null) {
                XuechengPlusException.cast("远程调用媒资服务上传文件失败");
            }
        } finally {
            try {
                file.delete();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public Boolean saveCourseIndex(Long courseId) {

        //取出课程发布信息
        CoursePublish coursePublish = coursePublishMapper.selectById(courseId);
        //拷贝至课程索引对象
        CourseIndex courseIndex = new CourseIndex();
        BeanUtils.copyProperties(coursePublish,courseIndex);
        //远程调用搜索服务api添加课程信息到索引
        Boolean add = searchServiceClient.add(courseIndex);
        if(!add){
            XuechengPlusException.cast("添加索引失败");
        }
        return add;

    }

    @Override
    public CoursePublish getCoursePublish(Long courseId) {
        return coursePublishMapper.selectById(courseId);
    }

    //保存课程发布信息到消息表
    private void saveCoursePublishMessage(Long courseId) {
        MqMessage course_publish = mqMessageService.addMessage("course_publish", String.valueOf(courseId), null, null);
        if(course_publish == null) {
            XuechengPlusException.cast(CommonError.UNKOWN_ERROR);
        }
    }
}
