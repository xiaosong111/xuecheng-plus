package com.xuecheng.content.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.base.model.RestResponse;
import com.xuecheng.content.model.dto.BindTeachplanMediaDto;
import com.xuecheng.content.model.dto.SaveTeachplanDto;
import com.xuecheng.content.model.dto.TeachplanDto;
import com.xuecheng.content.model.po.Teachplan;

import java.util.List;

/**
 * @Author QG
 * @Date 2023/2/17 9:55
 * @description 课程计划服务
 **/

public interface TeachPlanService {
    List<TeachplanDto> selectTrrNodes(Long courseId);

    void saveTeachPlan(SaveTeachplanDto saveTeachplanDto);

    void deleteTeachPlan(Long teachplanId);

    void moveTeachPlan(String moveType, Long teachId);

    /**
     * 绑定课程计划和媒资文件
     * @param bindTeachplanMediaDto
     */
    void associationMedia(BindTeachplanMediaDto bindTeachplanMediaDto);

    RestResponse deleteBindByTeachPlanMedia(String teachPlanId, String mediaId);
}
