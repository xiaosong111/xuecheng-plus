package com.xuecheng.content.mapper;

import com.xuecheng.content.model.po.CoursePublishPre;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 13302
* @description 针对表【course_publish_pre(课程发布)】的数据库操作Mapper
* @createDate 2023-03-11 11:07:39
* @Entity com.xuecheng.content.po.CoursePublishPre
*/
public interface CoursePublishPreMapper extends BaseMapper<CoursePublishPre> {

}




