package com.xuecheng.content.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xuecheng.content.mapper.TeachplanMapper;
import com.xuecheng.content.mapper.TeachplanMediaMapper;
import com.xuecheng.content.service.TeachPlanService;
import com.xuecheng.base.exception.CommonError;
import com.xuecheng.base.exception.XuechengPlusException;
import com.xuecheng.base.model.RestResponse;
import com.xuecheng.content.model.constants.TeachPlanConstants;
import com.xuecheng.content.model.dto.BindTeachplanMediaDto;
import com.xuecheng.content.model.dto.SaveTeachplanDto;
import com.xuecheng.content.model.dto.TeachplanDto;
import com.xuecheng.content.model.po.Teachplan;
import com.xuecheng.content.model.po.TeachplanMedia;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author QG
 * @Date 2023/2/17 9:56
 * @description 课程计划相关服务
 **/
@Service
@Slf4j
public class TeachPlanServiceImpl implements TeachPlanService {

    @Resource
    private TeachplanMapper teachplanMapper;
    @Resource
    private TeachplanMediaMapper teachplanMediaMapper;

    /**
     * 根据课程id获取课程的课程计划树形结果
     *
     * @param courseId
     * @return
     */
    @Override
    public List<TeachplanDto> selectTrrNodes(Long courseId) {
        return teachplanMapper.selectTreeNodes(courseId);
    }

    @Override
    public void saveTeachPlan(SaveTeachplanDto dto) {
        Long id = dto.getId();
        if (id == null) {
            //新增课程计划
            Teachplan teachplan = new Teachplan();
            BeanUtils.copyProperties(dto,teachplan);
            int count = getEqCount(dto.getCourseId(), dto.getParentid());
            teachplan.setOrderby(count + 1);
            teachplanMapper.insert(teachplan);

        } else {
            Teachplan teachplan = teachplanMapper.selectById(id);
            //修改课程计划
            BeanUtils.copyProperties(dto, teachplan);
            teachplanMapper.updateById(teachplan);
        }
    }


    //获取同级课程计划的数量
    private int getEqCount(Long courseId, Long parentid) {

        List<Integer> list = null;
        list = teachplanMapper.selectMaxOrder(courseId, parentid);
        if(list == null || list.size() == 0) return 0;
        list.sort((o1, o2) -> o2.compareTo(o1));
        return list.get(0);
    }

    @Override
    @Transactional
    public void deleteTeachPlan(Long teachplanId) {
        Teachplan teachplan = teachplanMapper.selectById(teachplanId);
        if(teachplan == null) {
            XuechengPlusException.cast("无对应课程计划");
        }
        if(teachplan.getParentid() == 0) {
            //查询根节点的子节点数
            LambdaQueryWrapper<Teachplan> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(Teachplan::getParentid,teachplanId);
            Integer count = teachplanMapper.selectCount(queryWrapper);
            if(count != 0) {
                XuechengPlusException.cast("课程计划还有子级信息，无法操作！");
                return;
            }
            teachplanMapper.deleteById(teachplanId);
        } else {
            //当前节点为子节点，删除自生以及对应的媒资信息
            teachplanMapper.deleteById(teachplanId);
            LambdaQueryWrapper<TeachplanMedia> qw = new LambdaQueryWrapper<>();
            qw.eq(TeachplanMedia::getTeachplanId,teachplanId);
            teachplanMediaMapper.delete(qw);
        }
    }

    @Override
    public void moveTeachPlan(String moveType, Long teachId) {
        if(moveType == null || moveType.equals("")) {
            XuechengPlusException.cast(CommonError.PARAMS_ERROR);
        }
        if(TeachPlanConstants.MOVE_UP.equals(moveType)) {
            //向上移动
            move(teachId,TeachPlanConstants.UP);
        } else if(TeachPlanConstants.MOVE_DOWN.equals(moveType)){
            //向下移动
            move(teachId,TeachPlanConstants.DOWN);
        } else {
            XuechengPlusException.cast(CommonError.PARAMS_ERROR);
        }
    }

    @Override
    @Transactional
    public void associationMedia(BindTeachplanMediaDto bindTeachplanMediaDto) {
        //查询课程计划是否存在
        Long teachplanId = bindTeachplanMediaDto.getTeachplanId();
        Teachplan teachplan = teachplanMapper.selectById(teachplanId);
        if(teachplan == null) {
            XuechengPlusException.cast("参数错误，当前课程计划不存在");
        }
        //删除之前课程计划绑定的媒资文件
        LambdaQueryWrapper<TeachplanMedia> queryWrapper = new LambdaQueryWrapper<TeachplanMedia>().eq(TeachplanMedia::getTeachplanId, teachplanId);
        teachplanMediaMapper.delete(queryWrapper);
        //新增数据
        TeachplanMedia teachplanMedia = new TeachplanMedia();
        teachplanMedia.setMediaId(bindTeachplanMediaDto.getMediaId());
        teachplanMedia.setMediaFilename(bindTeachplanMediaDto.getFileName());
        teachplanMedia.setTeachplanId(teachplanId);
        teachplanMedia.setCreateDate(LocalDateTime.now());
        teachplanMedia.setCourseId(teachplan.getCourseId());
        teachplanMediaMapper.insert(teachplanMedia);
    }

    @Override
    public RestResponse deleteBindByTeachPlanMedia(String teachPlanId, String mediaId) {
        if(StringUtils.isEmpty(teachPlanId) || StringUtils.isEmpty(mediaId)) {
            XuechengPlusException.cast("参数不能为空");
        }
        //构造查询条件
        try {
            LambdaQueryWrapper<TeachplanMedia> queryWrapper = new LambdaQueryWrapper<TeachplanMedia>().eq(TeachplanMedia::getMediaId, mediaId).eq(TeachplanMedia::getTeachplanId, teachPlanId);
            teachplanMediaMapper.delete(queryWrapper);
        } catch (Exception e) {
            XuechengPlusException.cast("执行过程错误");
        }
        return RestResponse.success();
    }

    public void move(Long teachId,int moveFlag) {
        //查询当前课程计划
        Teachplan teachplan = teachplanMapper.selectById(teachId);
        List<Teachplan> res = null;
        res = teachplanMapper.selectMoveList(teachplan.getParentid(),teachplan.getOrderby(),teachplan.getCourseId(),moveFlag);
        if(res == null || res.size() == 0) {
            XuechengPlusException.cast("当前已经是最上或最下层了");
            return;
        }
        if(moveFlag == 0) {
            res.sort(((o1, o2) -> o2.getOrderby() - o1.getOrderby()));
        } else if(moveFlag == 1) {
            res.sort(((o1, o2) -> o1.getOrderby() - o2.getOrderby()));
        } else {
            XuechengPlusException.cast(CommonError.PARAMS_ERROR);
            return;
        }
        exchangeOrder(teachplan, res);
    }

    private void exchangeOrder(Teachplan teachplan, List<Teachplan> res) {
        //移动元素
        Teachplan teachplanmove = res.get(0);
        Integer orderby = teachplanmove.getOrderby();
        teachplanmove.setOrderby(teachplan.getOrderby());
        teachplan.setOrderby(orderby);
        teachplanMapper.updateById(teachplan);
        teachplanMapper.updateById(teachplanmove);
    }
}
