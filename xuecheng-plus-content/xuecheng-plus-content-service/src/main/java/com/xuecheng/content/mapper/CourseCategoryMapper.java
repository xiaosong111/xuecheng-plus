package com.xuecheng.content.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xuecheng.content.model.dto.CourseCategoryTreeDto;
import com.xuecheng.content.model.po.CourseCategory;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* @author 13302
* @description 针对表【course_category(课程分类)】的数据库操作Mapper
* @createDate 2023-02-16 09:57:20
* @Entity com.xuecheng.content.mapper.CourseCategory
*/
public interface CourseCategoryMapper extends BaseMapper<CourseCategory> {

    /**
     * 获取除根节点外的所有节点
     * @param id
     * @return
     */
    public List<CourseCategoryTreeDto> getAllExRoot(String id);
}




