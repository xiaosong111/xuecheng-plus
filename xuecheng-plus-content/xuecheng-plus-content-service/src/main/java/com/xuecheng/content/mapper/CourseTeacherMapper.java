package com.xuecheng.content.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xuecheng.content.model.po.CourseTeacher;

/**
* @author QG
* @description 针对表【course_teacher(课程-教师关系表)】的数据库操作Mapper
* @createDate 2023-02-19 11:46:22
* @Entity com.xuecheng.content.model.CourseTeacher
*/
public interface CourseTeacherMapper extends BaseMapper<CourseTeacher> {

}




