package com.xuecheng.content.service;

import com.xuecheng.content.model.po.CourseTeacher;

import java.util.List;

/**
 * @Author QG
 * @Date 2023/2/19 11:59
 * @description 
 **/
public interface CourseTeacherService {
    List<CourseTeacher> queryCourseTeacher(Long courseId);

    CourseTeacher saveOrUpdateCourseTeacher(CourseTeacher courseTeacher);

    void deleteCourseTeacher(String courseId, String teacherId);
}
