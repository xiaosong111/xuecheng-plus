package com.xuecheng.oss;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.GetObjectRequest;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.PutObjectRequest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.cassandra.CassandraProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.*;

/**
 * @Author QG
 * @Date 2023/2/21 19:12
 * @description oss服务测试
 **/
public class OSSTest {

    // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
    String endpoint = "oss-cn-hangzhou.aliyuncs.com";
    // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
    String accessKeyId = "LTAI5tFbGqXWFp2bWwa456DT";
    String accessKeySecret = "6K3Kd6pAgD94wLfuonm12QxJQKlkmo";
    // 填写Bucket名称，例如examplebucket。
    String bucketName = "qg-xuecheng-file";
    private static OSS ossClient = null;
     {
        // 创建OSSClient实例。
        ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
    }

    /**
     * 上传文件
     */
    @Test
    public void uploadFile() {
        try {
            // 创建PutObjectRequest对象。
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, "test/宋双江.pdf", new File("F:\\宋双江.pdf"));
            ossClient.putObject(putObjectRequest);
        } catch (OSSException e){
            e.printStackTrace();
        } finally {
            // 关闭OSSClient。
            ossClient.shutdown();
        }
        System.out.println("上传成功");
    }

    /**
     * 下载文件到本地
     */
    @Test
    public void download2Local() {
        try {
            ossClient.getObject(new GetObjectRequest(bucketName, "test/1.jpg"), new File("F:\\2.jpg"));
        } finally {
            if(ossClient != null) {
                ossClient.shutdown();
            }
        }
        System.out.println("下载成功");
    }


    /**
     * 下载文件到流中  (需使用字节流下载图片等文件)
     */
    @Test
    public void download2Stream() throws IOException {
        OSSObject object = ossClient.getObject(bucketName, "test/1.jpg");
        try(
                BufferedInputStream in = new BufferedInputStream(object.getObjectContent());
                BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream("F://1.jpg"));
                ) {
            byte[] bytes = new byte[1024];
            int len = 0;
            while ((len = in.read()) != -1) {
                writer.write(len);
            }
        } catch (OSSException | ClientException | IOException e) {
            throw new RuntimeException(e);
        } finally {
            ossClient.shutdown();
        }
        System.out.println("下载成功");
    }

    /**
     * 删除文件
     */
    @Test
    public void deleteFile() {
        try {
            ossClient.deleteObject(bucketName,"test/宋双江.pdf");
        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message: " + oe.getErrorMessage());
            System.out.println("Error Code:       " + oe.getErrorCode());
            System.out.println("Request ID:      " + oe.getRequestId());
            System.out.println("Host ID:           " + oe.getHostId());
        } catch (ClientException ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message: " + ce.getMessage());
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        System.out.println("删除成功！");
    }

}
