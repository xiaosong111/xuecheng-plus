package com.xuecheng.oss;

import io.minio.*;
import io.minio.errors.*;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @Author QG
 * @Date 2023/3/6 16:34
 * @description  minio文件操作功能测试
 **/
public class MinIOTest {

    private static MinioClient minioClient =
            MinioClient.builder()
            .endpoint("http://47.108.249.78:9000")
                    .credentials("xuecheng-admin","dyj3101631524")
                    .build();


    @Test
    public void uploadFile() throws IOException, ServerException, InsufficientDataException, ErrorResponseException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {

            UploadObjectArgs uploadObjectArgs =  UploadObjectArgs.builder()
                    .bucket("xuecheng-file")
                    .object("test.avi")
                    .filename("F:/test.avi")
                    .build();
            minioClient.uploadObject(uploadObjectArgs);
            System.out.println("success");

    }

    @Test
    public void downLoadFile() throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        InputStream stream = minioClient.getObject(
                GetObjectArgs.builder()
                        .bucket("xuecheng-file")
                        .object("test.avi")
                        .build());
        FileOutputStream fileOutputStream = new FileOutputStream("F://test.avi");
        IOUtils.copy(stream,fileOutputStream);
    }

}
