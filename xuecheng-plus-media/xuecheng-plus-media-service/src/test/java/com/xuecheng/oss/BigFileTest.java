package com.xuecheng.oss;

import org.jacoco.agent.rt.internal_43f5073.core.analysis.ICounter;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @Author QG
 * @Date 2023/2/28 8:54
 * @description
 **/
public class BigFileTest {

    /**
     * 测试文件分块方法
     */
    @Test
    public void testFileChunk() throws IOException {
        File sourceFile = new File("F:\\test\\english.mp4");
        File chunkFile = new File("F:\\test\\chunk\\");
        //定义分块大小
        long chunkSize = 1024 * 1024;
        //分块数量
        long chunkNum = (long) Math.ceil((sourceFile.length() * 1.0) / chunkSize);
        byte[] b = new byte[1024];
        int len = -1;
        RandomAccessFile ras_read = new RandomAccessFile(sourceFile, "r");
        for (int i = 0; i < chunkNum; i++) {
            //创建分块文件
            File file = new File("F:\\test\\chunk\\" + i);
            if(file.exists()) {
                file.delete();
            }
            boolean newFile = file.createNewFile();
            if(newFile) {
                RandomAccessFile ras_write = new RandomAccessFile(file,"rw");
                //向分块文件中写数据
                while ((len = ras_read.read(b)) != -1) {
                    ras_write.write(b,0,len);
                    if(file.length() >= chunkSize) {
                        break;
                    }
                }
                ras_write.close();
                System.out.println("分块完成" + i);
            }
        }
        ras_read.close();
    }

    @Test
    public void testFileMerge() throws IOException {
        //获取源文件
        File source_File = new File("F:\\test\\chunk\\");
        //获取目的文件路径
        File desFile = new File("F:\\test\\1.mp4");
        if(desFile.exists()) {
            desFile.delete();
        }
        boolean desFileNewFile = desFile.createNewFile();
        RandomAccessFile ras_write = new RandomAccessFile(desFile, "rw");
        //分块文件排序
        File[] files = source_File.listFiles();
        List<File> filesList = Arrays.asList(files);
        Collections.sort(filesList, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                return Integer.parseInt(o1.getName()) - Integer.parseInt(o2.getName());
            }
        });
        byte[] b = new byte[1024];
        int len = -1;
        int count = 1;
        for (File file : filesList) {
            //讲文件读取到目的文件中
            RandomAccessFile ras_read = new RandomAccessFile(file, "r");
            while((len = ras_read.read(b)) != -1) {
                ras_write.write(b,0,len);
            }
            System.out.println("分块合并完成"+ count);
            count++;
            ras_read.close();
        }
        ras_write.close();
    }
}
