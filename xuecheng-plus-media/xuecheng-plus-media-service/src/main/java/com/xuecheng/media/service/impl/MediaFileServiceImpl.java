package com.xuecheng.media.service.impl;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.GetObjectRequest;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectRequest;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.j256.simplemagic.ContentInfo;
import com.j256.simplemagic.ContentInfoUtil;
import com.xuecheng.base.exception.XuechengPlusException;
import com.xuecheng.base.model.PageParams;
import com.xuecheng.base.model.PageResult;
import com.xuecheng.base.model.RestResponse;
import com.xuecheng.media.mapper.MediaFilesMapper;
import com.xuecheng.media.mapper.MediaProcessMapper;
import com.xuecheng.media.model.dto.QueryMediaParamsDto;
import com.xuecheng.media.model.dto.UploadFileParamsDto;
import com.xuecheng.media.model.dto.UploadFileResultDto;
import com.xuecheng.media.model.po.MediaFiles;
import com.xuecheng.media.model.po.MediaProcess;
import com.xuecheng.media.service.MediaFileService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MimeType;

import java.io.*;
import java.lang.reflect.Proxy;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author Mr.M
 * @version 1.0
 * @description 媒资信息服务类
 * @date 2022/9/10 8:58
 */
@Slf4j
//@Service
public class MediaFileServiceImpl implements MediaFileService {

    @Autowired
    private MediaFilesMapper mediaFilesMapper;
    //oss对象服务类
    @Autowired
    private OSS ossClient;

    @Value("${oss.bucket.files}")
    private String bucket_file;

    @Autowired
    private MediaFileService current_proxy;

    @Autowired
    private MediaProcessMapper mediaProcessMapper;

    @Override
    public PageResult<MediaFiles> queryMediaFiels(Long companyId, PageParams pageParams, QueryMediaParamsDto queryMediaParamsDto) {

        //构建查询条件对象
        LambdaQueryWrapper<MediaFiles> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(queryMediaParamsDto.getFilename()), MediaFiles::getFilename, queryMediaParamsDto.getFilename());
        queryWrapper.eq(StringUtils.isNotEmpty(queryMediaParamsDto.getFileType()), MediaFiles::getFileType, queryMediaParamsDto.getFileType());
//        queryWrapper.eq(StringUtils.isNotEmpty(queryMediaParamsDto.getAuditStatus()), MediaFiles::getAuditStatus, queryMediaParamsDto.getAuditStatus());

        //分页对象
        Page<MediaFiles> page = new Page<>(pageParams.getPageNo(), pageParams.getPageSize());
        // 查询数据内容获得结果
        Page<MediaFiles> pageResult = mediaFilesMapper.selectPage(page, queryWrapper);
        // 获取数据列表
        List<MediaFiles> list = pageResult.getRecords();
        // 获取数据总数
        long total = pageResult.getTotal();
        // 构建结果集
        PageResult<MediaFiles> mediaListResult = new PageResult<>(list, total, pageParams.getPageNo(), pageParams.getPageSize());
        return mediaListResult;

    }

    @Override
    public UploadFileResultDto uploadFile(Long companyId, UploadFileParamsDto uploadFileParamsDto, byte[] bytes, String folder, String objectName) {
        String fileMd5 = DigestUtils.md5Hex(bytes);
        String filename = uploadFileParamsDto.getFilename();
        if (StringUtils.isEmpty(objectName)) {
            objectName = fileMd5 + filename.substring(filename.lastIndexOf("."));
        }

        if (StringUtils.isEmpty(folder)) {
            folder = getFileFolder(new Date(), true, true, true);
        } else if (!folder.contains("/")) {
            folder = folder + "/";
        }
        objectName = folder + objectName;
        //存储文件到oss中
        // 创建PutObjectRequest对象。
        try {
            addMediaFileToOss(bytes, bucket_file, objectName);
            //保存文件相关信息到数据库
            MediaFiles mediaFiles = current_proxy.addMediaFileToDb(companyId, uploadFileParamsDto, objectName, bucket_file, fileMd5);
            //返回文件信息
            UploadFileResultDto res = new UploadFileResultDto();
            BeanUtils.copyProperties(mediaFiles, res);
            return res;
        } catch (OSSException | BeansException | ClientException e) {
            XuechengPlusException.cast("文件上传过程异常");
        }
        return null;
    }

    /**
     * 上传文件到数据库
     *
     * @param companyId           机构id
     * @param uploadFileParamsDto 文件相关参数
     * @param objectName          文件名
     * @param fileMd5             文件md5 作为id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public MediaFiles addMediaFileToDb(Long companyId, UploadFileParamsDto uploadFileParamsDto, String objectName, String bucket_name, String fileMd5) {
        //根据文件名称取出媒体类型
        //扩展名
        String extension = null;
        if (objectName.indexOf(".") >= 0) {
            extension = objectName.substring(objectName.lastIndexOf("."));
        }
        //获取扩展名对应的媒体类型
        String contentType = getMimeTypeByExtension(extension);
        MediaFiles mediaFiles = new MediaFiles();
        BeanUtils.copyProperties(uploadFileParamsDto, mediaFiles);
        mediaFiles.setId(fileMd5);
        mediaFiles.setFileId(fileMd5);
        mediaFiles.setCompanyId(companyId);
        mediaFiles.setFilePath(objectName);
        //判断文件类型，确定是否设置url，如果是avi类型url设置为空，处理文件时在设置url
        if (contentType.indexOf("image") >= 0 || contentType.indexOf("mp4") >= 0) {
            mediaFiles.setUrl("/" + objectName);
        }
        mediaFiles.setBucket(bucket_name);
        mediaFiles.setCreateDate(LocalDateTime.now());
        mediaFiles.setStatus("1");
        //保存文件信息到文件表
        int insert = mediaFilesMapper.insert(mediaFiles);
        if (insert < 0) {
            XuechengPlusException.cast("保存文件信息失败");
        }
        //如果视频类型为avi，则添加吻技安信息到文件处理表中待处理
        if(contentType.equals("video/x-msvideo")) {
            MediaProcess mediaProcess = new MediaProcess();
            BeanUtils.copyProperties(mediaFiles,mediaProcess);
            mediaProcessMapper.insert(mediaProcess);
        }
        return mediaFiles;
    }

    /**
     * 根据文件扩展名获取对应的文件类型
     *
     * @param extension
     * @return
     */
    private String getMimeTypeByExtension(String extension) {
        String contentType = MediaType.APPLICATION_OCTET_STREAM_VALUE;
        if (StringUtils.isNotEmpty(extension)) {
            ContentInfo extensionMatch = ContentInfoUtil.findExtensionMatch(extension);
            if (extensionMatch != null) {
                contentType = extensionMatch.getMimeType();
            }
        }
        return contentType;

    }

    @Override
    public RestResponse<Boolean> checkFile(String fileMd5) {
        MediaFiles mediaFiles = mediaFilesMapper.selectById(fileMd5);
        if (mediaFiles != null) {
            String bucket = mediaFiles.getBucket();
            String filePath = mediaFiles.getFilePath();
            try {
                //根据数据库信息查询OSS服务
                OSSObject object = ossClient.getObject(bucket, filePath);
                InputStream objectContent = object.getObjectContent();
                if (objectContent != null) {
                    return RestResponse.success(true);
                }
            } catch (OSSException e) {
                return RestResponse.success(false);
            } catch (ClientException e) {
                throw new RuntimeException(e);
            }
        }
        return RestResponse.success(false);
    }

    @Override
    public RestResponse<Boolean> checkChunk(String fileMd5, int chunk) {
        //获取分块文件目录路径
        String chunkFolderFilePath = getChunkFilePath(fileMd5);
        //得到分块文件的路径
        String chunkFilePath = chunkFolderFilePath + chunk;
        InputStream objectContent = null;
        try {
            objectContent = ossClient.getObject(new GetObjectRequest(bucket_file, chunkFilePath)).getObjectContent();
            if (objectContent != null) {
                return RestResponse.success(true);
            }
        } catch (OSSException | ClientException e) {
            log.debug("检查分块文件不存在");
        }
        //分块文件不存在
        return RestResponse.success(false);
    }

    @Override
    public RestResponse uploadChunk(String fileMd5, int chunk, byte[] bytes) {
        //获取分块文件路径
        String chunkFilePath = getChunkFilePath(fileMd5) + chunk;

        try {
            addMediaFileToOss(bytes, bucket_file, chunkFilePath);
            return RestResponse.success(true);
        } catch (Exception e) {
            log.debug("上传分块文件{}错误,文件md5：{}", chunk, fileMd5);
        }
        return RestResponse.success(false);
    }

    @Override
    public RestResponse mergeChunks(Long companyId, String fileMd5, int chunkTotal, UploadFileParamsDto uploadFileParamsDto) {
        //合并前下载分块
        File[] chunkFiles = checkChunkStatus(fileMd5, chunkTotal);
        //扩展名
        String fileName = uploadFileParamsDto.getFilename();
        String extName = fileName.substring(fileName.lastIndexOf("."));
        //创建临时文件作为合并文件
        File mergeFile = null;
        try {
            mergeFile = File.createTempFile(fileMd5, extName);
        } catch (IOException e) {
            XuechengPlusException.cast("合并文件过程中创建临时文件出错");
        }
        //开始合并
        try {
            byte[] b = new byte[1024];
            try (
                    RandomAccessFile write = new RandomAccessFile(mergeFile, "rw")
            ) {
                for (File chunkFile : chunkFiles) {
                    int len = 0;
                    try (
                            RandomAccessFile read = new RandomAccessFile(chunkFile, "r");
                    ) {
                        while ((len = read.read(b)) != -1) {
                            write.write(b, 0, len);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                XuechengPlusException.cast("合并文件过程中出错");
            }
            log.debug("文件合并成功 {}", mergeFile.getAbsolutePath());
            uploadFileParamsDto.setFileSize(mergeFile.length());
            //对文件进行校验
            try (
                    FileInputStream fileInputStream = new FileInputStream(mergeFile)
            ) {
                String md5Hex = DigestUtils.md5Hex(fileInputStream);
                if (!fileMd5.equals(md5Hex)) {
                    XuechengPlusException.cast("合并文件校验失败");
                }
                log.debug("合并文件校验成功");
            } catch (IOException e) {
                e.printStackTrace();
                XuechengPlusException.cast("合并文件校验异常");
            }
            //上传合并后的文件到OSS
            String mergeFilePath = getFilePathByMd5(fileMd5, extName);
            try {
                addMediaFileToOss(mergeFile.getAbsolutePath(), bucket_file, mergeFilePath);
                log.debug("合并文件上传成功");
            } catch (Exception e) {
                e.printStackTrace();
                XuechengPlusException.cast("合并文件时上传文件出错");
            }
            //文件信息入库
            MediaFiles mediaFiles = null;
            try {
                mediaFiles = current_proxy.addMediaFileToDb(companyId, uploadFileParamsDto, mergeFilePath, bucket_file, fileMd5);
            } catch (Exception e) {
                log.debug("文件信息入库异常",e.toString(),e.getMessage());
            }
            if (mediaFiles == null) {
                XuechengPlusException.cast("媒资文件入库出错");
            }
            return RestResponse.success(true);
        } finally {
            //删除临时文件
            try {
                for (File chunkFile : chunkFiles) {
                    chunkFile.delete();
                }
            } catch (Exception e) {
            }
            //删除合并文件
            try {
                mergeFile.delete();
            } catch (Exception e) {

            }
        }
    }

    @Override
    public RestResponse<String> getPlayUrlByMediaId(String mediaId) {
        //根据文件id查询文件
        if (StringUtils.isEmpty(mediaId)) {
            XuechengPlusException.cast("参数错误");
        }
        MediaFiles mediaFiles = mediaFilesMapper.selectById(mediaId);
        if (StringUtils.isEmpty(mediaFiles.getUrl())) {
            XuechengPlusException.cast("视频还没有转码处理");
        }
        return RestResponse.success(mediaFiles.getUrl());
    }

    /**
     * 根据文件绝对路径获取文件，然后将文件上传到OSS
     *
     * @param absolutePath
     * @param bucket_file
     * @param mergeFilePath
     */
    public void addMediaFileToOss(String absolutePath, String bucket_file, String mergeFilePath) {
        try (
                FileInputStream fileInputStream = new FileInputStream(new File(absolutePath));
        ) {
            String extension = mergeFilePath.substring(mergeFilePath.lastIndexOf("."));
            String mimeTypeByExtension = getMimeTypeByExtension(extension);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentType(mimeTypeByExtension);
            ossClient.putObject(new PutObjectRequest(bucket_file, mergeFilePath, fileInputStream, objectMetadata));
        } catch (IOException e) {
            e.printStackTrace();
            XuechengPlusException.cast("上传文件到文件系统出错");
        }
    }

    /**
     * 根据文件md5值和扩展名获取其上传后的路径（上传的是合并后的大文件）
     *
     * @param fileMd5
     * @param extName
     * @return
     */
    private String getFilePathByMd5(String fileMd5, String extName) {
        return fileMd5.substring(0, 1) + "/" + fileMd5.substring(1, 2) + "/" + fileMd5 + "/" + fileMd5 + extName;
    }

    //下载分块文件并返回
    private File[] checkChunkStatus(String fileMd5, int chunkTotal) {
        File[] chunkFiles = new File[chunkTotal];
        //获取分块文件目录路径
        String chunkFileFolderPath = getChunkFilePath(fileMd5);
        for (int i = 0; i < chunkTotal; i++) {
            String chunkFilePath = chunkFileFolderPath + i;
            //下载文件
            File chunkFile = null;
            try {
                chunkFile = File.createTempFile("chunk" + i, null);
            } catch (IOException e) {
                e.printStackTrace();
                XuechengPlusException.cast("下载分块时创建临时文件出错");
            }
            downloadFileFromOSS(chunkFile, chunkFilePath, bucket_file);
            chunkFiles[i] = chunkFile;
        }
        return chunkFiles;
    }

    /**
     * 从OSS下载分块文件到chunFile中
     *
     * @param chunkFile     接受oss的分块文件
     * @param chunkFilePath 分块文件路径名
     * @param bucket_file   桶名
     */
    public File downloadFileFromOSS(File chunkFile, String chunkFilePath, String bucket_file) {
        OSSObject object = ossClient.getObject(new GetObjectRequest(bucket_file, chunkFilePath));
        try (
                InputStream objectContent = object.getObjectContent();
                OutputStream outputStream = Files.newOutputStream(chunkFile.toPath());
        ) {
            IOUtils.copy(objectContent, outputStream);
        } catch (Exception e) {
            log.debug("下载分块文件出错 文件：{}", chunkFilePath);
            XuechengPlusException.cast("下载文件" + chunkFilePath + "出错");
        }
        return chunkFile;
    }


    //根据md值获取分块文件目录的路径
    private String getChunkFilePath(String fileMd5) {
        return fileMd5.substring(0, 1) + "/" + fileMd5.substring(1, 2) + "/" + fileMd5 + "/chunk/";
    }

    /**
     * 上传文件到oss
     *
     * @param bytes
     * @param bucket_name
     * @param objectName
     */
    private void addMediaFileToOss(byte[] bytes, String bucket_name, String objectName) {
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucket_name, objectName, new ByteArrayInputStream(bytes));
        ossClient.putObject(putObjectRequest);
    }

    //生成文件路径，根据日期
    private String getFileFolder(Date date, boolean year, boolean month, boolean day) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String format = sdf.format(date);
        String[] split = format.split("-");
        StringBuffer sb = new StringBuffer();
        if (year) {
            sb.append(split[0]);
            sb.append("/");
        }
        if (month) {
            sb.append(split[1]);
            sb.append("/");
        }
        if (day) {
            sb.append(split[2]);
            sb.append("/");
        }
        return sb.toString();
    }
}
