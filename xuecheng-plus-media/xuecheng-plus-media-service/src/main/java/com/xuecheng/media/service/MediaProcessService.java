package com.xuecheng.media.service;

import com.xuecheng.media.model.po.MediaProcess;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author QG
 * @Date 2023/3/4 15:16
 * @description
 **/
public interface MediaProcessService {


    /**
     * @param shardIndex 分片序号
     * @param shardTotal 分片总数
     * @param count      获取记录数
     * @return java.util.List<com.xuecheng.media.model.po.MediaProcess>
     * @description 获取待处理任务
     */
    List<MediaProcess> getMediaProcessList(int shardIndex, int shardTotal, int count);

    /**
     * @param taskId   任务id
     * @param status   任务状态
     * @param fileId   文件id
     * @param url      url
     * @param errorMsg 错误信息
     * @return void
     * @description 保存任务结果
     */
    void saveProcessFinishStatus(Long taskId, String status, String fileId, String url, String errorMsg);

    /**
     * 根据分片总数和分片索引获取视频任务列表
     *
     * @param shareTotal 分片总数
     * @param shareIndex 分片索引
     * @param count      任务数
     * @return
     */
    List<MediaProcess> selectListByShardIndex(int shareTotal, int shareIndex, int count);

}
