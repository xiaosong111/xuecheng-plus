package com.xuecheng.media.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author QG
 * @Date 2023/3/6 17:39
 * @description
 **/
@Configuration
public class MinIOConfig {

    // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
    @Value("${minio.endpoint}")
    private String endpoint;
    // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
    @Value("${minio.accessKey}")
    String accessKeyId;
    //阿里云对象服务账号密码
    @Value("${minio.secretKey}")
    String accessKeySecret;


    @Bean
    public MinioClient minioClient() {
        // 创建OSSClient实例。
        return new MinioClient.Builder().endpoint(endpoint).credentials(accessKeyId,accessKeySecret).build();
    }


}
