package com.xuecheng.media.service.jobhandler;

import com.xuecheng.base.utils.Mp4VideoUtil;
import com.xuecheng.media.mapper.MediaProcessMapper;
import com.xuecheng.media.model.po.MediaProcess;
import com.xuecheng.media.service.MediaFileService;
import com.xuecheng.media.service.MediaProcessService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @Author QG
 * @Date 2023/3/4 18:49
 * @description 视频处理任务 用于xxl-job调度任务
 **/
@Slf4j
@Component
public class VideoTask {

    @Autowired
    private MediaProcessService mediaProcessService;
    @Autowired
    private MediaFileService mediaFileService;

    /**
     * 任务数
     */
    private static final int TASK_COUNT = 2;

    @Value("${videoprocess.ffmpegpath}")
    private String ffmpegpath;

    /**
     * 处理视频，将oss中的avi文件转换为MP4文件
     *
     * @throws Exception
     */
    @XxlJob("videoJobHandler")
    public void videoJobHandler() throws Exception {
        // 分片参数
        int shardIndex = XxlJobHelper.getShardIndex();
        int shardTotal = XxlJobHelper.getShardTotal();

        //获取任务的执行列表
        List<MediaProcess> taskList = null;
        try {
            taskList = mediaProcessService.selectListByShardIndex(shardTotal, shardIndex, TASK_COUNT);
        } catch (Exception e) {
            log.debug("取出任务异常");
            log.debug("异常信息：{}",e.toString());
            return;
        }
        if (taskList == null || taskList.size() == 0) {
            return;
        }
        int size = taskList.size();
        ExecutorService threadPool = Executors.newFixedThreadPool(size);
        CountDownLatch countDownLatch = new CountDownLatch(size);
        taskList.forEach(mediaProcess -> {
            threadPool.execute(() -> {
                File mp4File = null;
                File originFile = null;
                try {
                    //处理视频逻辑
                    String filePath = mediaProcess.getFilePath();
                    //原始md5
                    String fileId = mediaProcess.getFileId();
                    //原始文件名
                    String filename = mediaProcess.getFilename();
                    String bucket = mediaProcess.getBucket();
                    if("2".equals(mediaProcess.getStatus())) {
                        return;
                    }

                    //将视频下载到本地
                    try {
                        originFile = File.createTempFile("original", null);
                        mp4File = File.createTempFile("mp4", ".mp4");
                    } catch (IOException e) {
                        log.debug("创建临时文件出错");
                        countDownLatch.countDown();
                        return;
                    }
                    try {
                        mediaFileService.downloadFileFromOSS(originFile, filePath, bucket);
                    } catch (Exception e) {
                        log.debug("下载原始文件出错");
                        countDownLatch.countDown();
                        return;
                    }
                    //使用工具类转码
                    //准备工具类
                    String s = null;
                    try {
                        Mp4VideoUtil mp4VideoUtil = new Mp4VideoUtil(ffmpegpath, originFile.getAbsolutePath(), mp4File.getName(), mp4File.getAbsolutePath());
                        s = mp4VideoUtil.generateMp4();
                    } catch (Exception e) {
                        log.debug("文件格式转换出现异常，文件信息：{}", originFile.getAbsolutePath());
                        countDownLatch.countDown();
                        return;
                    }
                    if (!"success".equals(s)) {
                        //记录错误信息
                        log.error("处理视频失败,视频地址:{},错误信息:{}", bucket + filePath, s);
                        mediaProcessService.saveProcessFinishStatus(mediaProcess.getId(), "3", fileId, null, s);
                        countDownLatch.countDown();
                        return;
                    }

                    //将转码后的视频上传到oss
                    String objectName = null;
                    try {
                        objectName = getFilePath(fileId, ".mp4");
                        mediaFileService.addMediaFileToOss(mp4File.getAbsolutePath(), bucket, objectName);
                    } catch (Exception e) {
                        log.debug("上传文件到oss异常,视频地址{}，异常信息{}", objectName, e.toString());
                        countDownLatch.countDown();
                        return;
                    }

                    //保存历史操作记录
                    try {
                        mediaProcessService.saveProcessFinishStatus(mediaProcess.getId(), "2", fileId, objectName, null);
                    } catch (Exception e) {
                        log.debug("保存历史操作出错");
                        log.debug("异常信息：{}",e.toString(),e.getMessage());
                        countDownLatch.countDown();
                        return;
                    }
                    countDownLatch.countDown();
                } finally {
                    try {
                        originFile.delete();
                    } catch (Exception e) {
                    }
                    try {
                        mp4File.delete();
                    } catch (Exception e) {

                    }
                }
            });
        });
        //等待,给一个充裕的超时时间,防止无限等待，到达超时时间还没有处理完成则结束任务
        countDownLatch.await(30L, TimeUnit.MINUTES);

    }

    //根据文件md5和后缀名生成文件路径
    private String getFilePath(String fileMd5, String fileExt) {
        return fileMd5.substring(0, 1) + "/" + fileMd5.substring(1, 2) + "/" + fileMd5 + "/" + fileMd5 + fileExt;
    }

}
