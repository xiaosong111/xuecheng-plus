package com.xuecheng.media.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.xuecheng.base.exception.XuechengPlusException;
import com.xuecheng.media.mapper.MediaFilesMapper;
import com.xuecheng.media.mapper.MediaProcessHistoryMapper;
import com.xuecheng.media.mapper.MediaProcessMapper;
import com.xuecheng.media.model.po.MediaFiles;
import com.xuecheng.media.model.po.MediaProcess;
import com.xuecheng.media.model.po.MediaProcessHistory;
import com.xuecheng.media.service.MediaProcessService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author QG
 * @Date 2023/3/4 15:17
 * @description
 **/
@Slf4j
@Service
public class MediaProcessServiceImpl implements MediaProcessService {

    @Autowired
    private MediaProcessMapper mediaProcessMapper;
    @Autowired
    private MediaFilesMapper mediaFilesMapper;
    @Autowired
    private MediaProcessHistoryMapper mediaProcessHistoryMapper;

    @Override
    public List<MediaProcess> getMediaProcessList(int shardIndex, int shardTotal, int count) {
        return mediaProcessMapper.selectListByShardIndex(shardTotal,shardIndex,count);
    }

    @Override
    public void saveProcessFinishStatus(Long taskId, String status, String fileId, String url, String errorMsg) {
        //查询任务
        MediaProcess mediaProcess = mediaProcessMapper.selectById(taskId);
        if(mediaProcess == null) {
            //任务不存在，直接返回
            return;
        }
        //判断任务状态为成功或者失败
        if("3".equals(status)) {
            //任务状态为失败
            MediaProcess mediaProcessF = new MediaProcess();
            mediaProcessF.setStatus(status);
            mediaProcessF.setErrormsg(errorMsg);
            mediaProcessMapper.update(mediaProcessF, Wrappers.<MediaProcess>update().eq("id",taskId));
            return;
        }
        //处理任务成功
        //更新文件信息的状态
        MediaFiles mediaFiles = mediaFilesMapper.selectById(fileId);
        if(mediaFiles != null) {
            mediaFiles.setUrl(url);
            mediaFilesMapper.updateById(mediaFiles);
        }
        MediaProcess mediaRes = mediaProcessMapper.selectById(taskId);
        mediaRes.setStatus("2");
        mediaRes.setUrl(url);
        //保存记录到历史表中
        MediaProcessHistory mediaProcessHistory = new MediaProcessHistory();
        BeanUtils.copyProperties(mediaRes,mediaProcessHistory);
        mediaProcessHistoryMapper.insert(mediaProcessHistory);
        //删除文件处理表中的数据
        mediaProcessMapper.deleteById(taskId);
    }

    @Override
    public List<MediaProcess> selectListByShardIndex(int shareTotal, int shareIndex, int count) {
        return mediaProcessMapper.selectListByShardIndex(shareTotal,shareIndex,count);
    }


}
