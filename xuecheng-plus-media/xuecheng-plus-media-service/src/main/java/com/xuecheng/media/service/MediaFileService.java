package com.xuecheng.media.service;

import com.xuecheng.base.model.PageParams;
import com.xuecheng.base.model.PageResult;
import com.xuecheng.base.model.RestResponse;
import com.xuecheng.media.model.dto.QueryMediaParamsDto;
import com.xuecheng.media.model.dto.UploadFileParamsDto;
import com.xuecheng.media.model.dto.UploadFileResultDto;
import com.xuecheng.media.model.po.MediaFiles;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.File;
import java.util.List;

/**
 * @author QG
 * @version 1.0
 * @description 媒资文件管理业务类
 * @date 2022/9/10 8:55
 */
public interface MediaFileService {

    /**
     * @param pageParams          分页参数
     * @param queryMediaParamsDto 查询条件
     * @return com.xuecheng.base.model.PageResult<com.xuecheng.media.model.po.MediaFiles>
     * @description 媒资文件查询方法
     * @author Mr.M
     * @date 2022/9/10 8:57
     */
    PageResult<MediaFiles> queryMediaFiels(Long companyId, PageParams pageParams, QueryMediaParamsDto queryMediaParamsDto);

    /**
     * 上传文件
     *
     * @param companyId           机构id
     * @param uploadFileParamsDto 上传文件的相关参数
     * @param bytes               文件转换的字节数组
     * @param folder              文件路径
     * @param objectName          文件名
     * @return
     */
    UploadFileResultDto uploadFile(Long companyId, UploadFileParamsDto uploadFileParamsDto, byte[] bytes, String folder, String objectName);


    /**
     * 保存文件信息到数据库
     *
     * @param companyId
     * @param uploadFileParamsDto
     * @param objectName
     * @param bucket_name
     * @param fileMd5
     * @return
     */
    MediaFiles addMediaFileToDb(Long companyId, UploadFileParamsDto uploadFileParamsDto, String objectName, String bucket_name, String fileMd5);

    /**
     * 大文件上传前检查，检查文件是否存在，即文件信息是否入库
     *
     * @param fileMd5
     * @return
     */
    RestResponse<Boolean> checkFile(String fileMd5);

    /**
     * 分块文件上传前的检查，检查当前分块文件是否存在
     *
     * @param fileMd5
     * @param chunk
     * @return
     */
    RestResponse<Boolean> checkChunk(String fileMd5, int chunk);

    /**
     * 上传文件分块
     *
     * @param fileMd5 文件md5值
     * @param chunk   分块号
     * @param bytes   文件字节数组
     * @return
     */
    RestResponse uploadChunk(String fileMd5, int chunk, byte[] bytes);

    /**
     * @param companyId           机构id
     * @param fileMd5             文件md5
     * @param chunkTotal          分块总和
     * @param uploadFileParamsDto 文件信息
     * @return com.xuecheng.base.model.RestResponse
     * @description 合并分块
     */
    RestResponse mergeChunks(Long companyId, String fileMd5, int chunkTotal, UploadFileParamsDto uploadFileParamsDto);

    /**
     * 预览文件，返回文件的url
     *
     * @param mediaId 文件id
     * @return
     */
    RestResponse<String> getPlayUrlByMediaId(String mediaId);


    /**
     * 根据文件绝对路径获取文件，然后将文件上传到OSS
     *
     * @param absolutePath
     * @param bucket_file
     * @param mergeFilePath
     */
    void addMediaFileToOss(String absolutePath, String bucket_file, String mergeFilePath);


    /**
     * 从OSS下载分块文件到chunFile中
     *
     * @param chunkFile     接受oss的分块文件
     * @param chunkFilePath 分块文件路径名
     * @param bucket_file   桶名
     */
    File downloadFileFromOSS(File chunkFile, String chunkFilePath, String bucket_file);
}
