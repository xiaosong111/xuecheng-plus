package com.xuecheng.media.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xuecheng.media.model.po.MediaProcess;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author itcast
 */
public interface MediaProcessMapper extends BaseMapper<MediaProcess> {

    /**
     * 根据分片总数和分片索引获取视频任务列表
     * @param shareTotal  分片总数
     * @param shareIndex 分片索引
     * @param count 任务数
     * @return
     */
    public List<MediaProcess> selectListByShardIndex(@Param("shareTotal") int shareTotal,@Param("shareIndex") int shareIndex,@Param("count") int count);

}
