package com.xuecheng.media.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.j256.simplemagic.ContentInfo;
import com.j256.simplemagic.ContentInfoUtil;
import com.xuecheng.base.exception.XuechengPlusException;
import com.xuecheng.base.model.PageParams;
import com.xuecheng.base.model.PageResult;
import com.xuecheng.base.model.RestResponse;
import com.xuecheng.media.mapper.MediaFilesMapper;
import com.xuecheng.media.mapper.MediaProcessMapper;
import com.xuecheng.media.model.dto.QueryMediaParamsDto;
import com.xuecheng.media.model.dto.UploadFileParamsDto;
import com.xuecheng.media.model.dto.UploadFileResultDto;
import com.xuecheng.media.model.po.MediaFiles;
import com.xuecheng.media.model.po.MediaProcess;
import com.xuecheng.media.service.MediaFileService;
import io.minio.*;
import io.minio.errors.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @Author QG
 * @Date 2023/3/6 17:30
 * @description 文件上传minio实现
 **/
@Slf4j
@Service
@Primary
public class MediaFileServiceByMinIOImpl implements MediaFileService {

    @Autowired
    private MediaFilesMapper mediaFilesMapper;
    @Autowired
    private MinioClient minioClient;
    @Value("${minio.bucket.videofiles}")
    private String bucket_video;
    @Value("${minio.bucket.files}")
    private String bucket_file;
    @Autowired
    private MediaFileService currentProxy;
    @Autowired
    private MediaProcessMapper mediaProcessMapper;

    @Override
    public PageResult<MediaFiles> queryMediaFiels(Long companyId, PageParams pageParams, QueryMediaParamsDto queryMediaParamsDto) {
        //构建查询条件对象
        LambdaQueryWrapper<MediaFiles> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(queryMediaParamsDto.getFilename()), MediaFiles::getFilename, queryMediaParamsDto.getFilename());
        queryWrapper.eq(StringUtils.isNotEmpty(queryMediaParamsDto.getFileType()), MediaFiles::getFileType, queryMediaParamsDto.getFileType());
//        queryWrapper.eq(StringUtils.isNotEmpty(queryMediaParamsDto.getAuditStatus()), MediaFiles::getAuditStatus, queryMediaParamsDto.getAuditStatus());

        //分页对象
        Page<MediaFiles> page = new Page<>(pageParams.getPageNo(), pageParams.getPageSize());
        // 查询数据内容获得结果
        Page<MediaFiles> pageResult = mediaFilesMapper.selectPage(page, queryWrapper);
        // 获取数据列表
        List<MediaFiles> list = pageResult.getRecords();
        // 获取数据总数
        long total = pageResult.getTotal();
        // 构建结果集
        PageResult<MediaFiles> mediaListResult = new PageResult<>(list, total, pageParams.getPageNo(), pageParams.getPageSize());
        return mediaListResult;
    }

    @Override
    public UploadFileResultDto uploadFile(Long companyId, UploadFileParamsDto uploadFileParamsDto, byte[] bytes, String folder, String objectName) {
        String fileMd5 = DigestUtils.md5Hex(bytes);
        String filename = uploadFileParamsDto.getFilename();
        if (StringUtils.isEmpty(objectName)) {
            objectName = fileMd5 + filename.substring(filename.lastIndexOf("."));
        }

        if (StringUtils.isEmpty(folder)) {
            folder = getFileFolder(new Date(), true, true, true);
        } else if (!folder.contains("/")) {
            folder = folder + "/";
        }
        objectName = folder + objectName;
        //存储文件到oss中
        // 创建PutObjectRequest对象。
        try {
            addMediaFileToOss(bytes, bucket_file, objectName);
            //保存文件相关信息到数据库
            MediaFiles mediaFiles = currentProxy.addMediaFileToDb(companyId, uploadFileParamsDto, objectName, bucket_file, fileMd5);
            //返回文件信息
            UploadFileResultDto res = new UploadFileResultDto();
            BeanUtils.copyProperties(mediaFiles, res);
            return res;
        } catch (Exception e) {
            XuechengPlusException.cast("文件上传过程异常");
        }
        return null;
    }

    private String getFileFolder(Date date, boolean year, boolean month, boolean day) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String format = sdf.format(date);
        String[] split = format.split("-");
        StringBuffer sb = new StringBuffer();
        if (year) {
            sb.append(split[0]);
            sb.append("/");
        }
        if (month) {
            sb.append(split[1]);
            sb.append("/");
        }
        if (day) {
            sb.append(split[2]);
            sb.append("/");
        }
        return sb.toString();
    }

    @Override
    @Transactional
    public MediaFiles addMediaFileToDb(Long companyId, UploadFileParamsDto uploadFileParamsDto, String objectName, String bucket_name, String fileId) {
        String extension = null;
        if(objectName.indexOf(".") >= 0) {
            extension = objectName.substring(objectName.lastIndexOf("."));
        }
        String contentType = getMimeTypeByExtension(extension);

        //保存到数据库
        MediaFiles mediaFiles = mediaFilesMapper.selectById(fileId);
        if (mediaFiles == null) {
            mediaFiles = new MediaFiles();

            //封装数据
            BeanUtils.copyProperties(uploadFileParamsDto, mediaFiles);
            mediaFiles.setId(fileId);
            mediaFiles.setFileId(fileId);
            mediaFiles.setCompanyId(companyId);
            mediaFiles.setBucket(bucket_name);
            mediaFiles.setFilePath(objectName);
            //图片及mp4文件设置url
            if(contentType.indexOf("image")>=0 || contentType.indexOf("mp4")>=0){
                mediaFiles.setUrl("/" + bucket_name + "/" + objectName);
            }
            mediaFiles.setUrl("/" + bucket_name + "/" + objectName);
            mediaFiles.setCreateDate(LocalDateTime.now());
            mediaFiles.setStatus("1");
            mediaFiles.setAuditStatus("002003");
            //插入文件表
            int insert = mediaFilesMapper.insert(mediaFiles);
            if (insert < 0) {
                XuechengPlusException.cast("保存文件信息失败");
            }
            //如果视频类型为avi，则添加吻技安信息到文件处理表中待处理
            if(contentType.equals("video/x-msvideo")) {
                MediaProcess mediaProcess = new MediaProcess();
                BeanUtils.copyProperties(mediaFiles,mediaProcess);
                mediaProcessMapper.insert(mediaProcess);
            }
        }
        return mediaFiles;
    }


    @Override
    public RestResponse<Boolean> checkFile(String fileMd5) {
        MediaFiles mediaFiles = mediaFilesMapper.selectById(fileMd5);
        if (mediaFiles == null) {
            //当前文件不存在
            return RestResponse.success(false);
        }
        String bucket = mediaFiles.getBucket();
        String filePath = mediaFiles.getFilePath();
        try {
            InputStream object = minioClient.getObject(GetObjectArgs.builder().bucket(bucket).object(filePath).build());
            if (object != null) {
                return RestResponse.success(true);
            }
        } catch (Exception e) {
        }
        return RestResponse.success(false);
    }

    @Override
    public RestResponse<Boolean> checkChunk(String fileMd5, int chunkIndex) {
        //得到分块文件目录
        String chunkFileFolderPath = getChunkFileFolderPath(fileMd5);
        //得到分块文件的路径
        String chunkFilePath = chunkFileFolderPath + chunkIndex;
        InputStream fileInputStream = null;
        try {
            fileInputStream = minioClient.getObject(GetObjectArgs.builder()
                    .bucket(bucket_video)
                    .object(chunkFilePath)
                    .build());
            if (fileInputStream != null) {
                return RestResponse.success(true);
            }
        } catch (Exception e) {
        }
        return RestResponse.success(false);
    }

    //根据文件md5值获取分块文件夹路径
    private String getChunkFileFolderPath(String fileMd5) {
        return fileMd5.substring(0, 1) + "/" + fileMd5.substring(1, 2) + "/" + fileMd5 + "/" + "chunk" + "/";
    }

    @Override
    public RestResponse uploadChunk(String fileMd5, int chunk, byte[] bytes) {
        //获取分块文件路径
        String chunkFileFolderPath = getChunkFileFolderPath(fileMd5);
        String objectName = chunkFileFolderPath + chunk;
        //上传分块文件
        try {
            addMediaFileToOss(bytes, bucket_video, objectName);
            return RestResponse.success(true);
        } catch (Exception e) {
            log.debug("上传分块文件异常：{} {}", e.toString(), e.getMessage());
        }
        return RestResponse.success(false, "上传分块文件失败");
    }

    //将文件上传到分布式文件系统
    private void addMediaFileToOss(byte[] bytes, String bucket, String objectName) {

        //资源的媒体类型
        String contentType = MediaType.APPLICATION_OCTET_STREAM_VALUE;//默认未知二进制流

        if (objectName.indexOf(".") >= 0) {
            //取objectName中的扩展名
            String extension = objectName.substring(objectName.lastIndexOf("."));
            ContentInfo extensionMatch = ContentInfoUtil.findExtensionMatch(extension);
            if (extensionMatch != null) {
                contentType = extensionMatch.getMimeType();
            }

        }
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .bucket(bucket)
                    .object(objectName)
                    //InputStream stream, long objectSize 对象大小, long partSize 分片大小(-1表示5M,最大不要超过5T，最多10000)
                    .stream(byteArrayInputStream, byteArrayInputStream.available(), -1)
                    .contentType(contentType)
                    .build();
            //上传到minio
            minioClient.putObject(putObjectArgs);
        } catch (Exception e) {
            e.printStackTrace();
            log.debug("上传文件到文件系统出错:{}", e.getMessage());
            XuechengPlusException.cast("上传文件到文件系统出错");
        }
    }

    @Override
    public RestResponse mergeChunks(Long companyId, String fileMd5, int chunkTotal, UploadFileParamsDto uploadFileParamsDto) {
        String filename = uploadFileParamsDto.getFilename();
        //下载所有分块文件
        File[] chunkFiles = checkChunkStatus(fileMd5, chunkTotal);
        //合并分块文件
        String extName = filename.substring(filename.lastIndexOf("."));
        File merge = null;
        try {
            try {
                merge = File.createTempFile("merge", extName);
            } catch (IOException e) {
                e.printStackTrace();
                XuechengPlusException.cast("创建临时合并文件失败");
            }
            try (
                    RandomAccessFile ras_write = new RandomAccessFile(merge, "rw");
            ) {
                byte[] bytes = new byte[1024];
                for (File chunkFile : chunkFiles) {
                    int len = 0;
                    //依次合并分块文件
                    try (
                            RandomAccessFile ras_read = new RandomAccessFile(chunkFile, "r");
                    ) {
                        while ((len = ras_read.read(bytes)) != -1) {
                            ras_write.write(bytes, 0, len);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                XuechengPlusException.cast("合并分块文件时异常");
            }
            log.debug("文件合并完成{}", merge.getAbsolutePath());
            uploadFileParamsDto.setFileSize(merge.length());
            //校验文件md5值
            try (InputStream mergeFileInputStream = new FileInputStream(merge);) {
                //对文件进行校验，通过比较md5值
                String newFileMd5 = DigestUtils.md5Hex(mergeFileInputStream);
                if (!fileMd5.equalsIgnoreCase(newFileMd5)) {
                    //校验失败
                    XuechengPlusException.cast("合并文件校验失败");
                }
                log.debug("合并文件校验通过{}", merge.getAbsolutePath());
            } catch (Exception e) {
                e.printStackTrace();
                //校验失败
                XuechengPlusException.cast("合并文件校验异常");
            }

            //上传合并文件
            String mergePath = getMergeFileFolderPath(fileMd5, extName);
            try {
                addMediaFileToOss(merge.getAbsolutePath(), bucket_video, mergePath);
            } catch (Exception e) {
                e.printStackTrace();
                XuechengPlusException.cast("上传合并文件异常");
            }
            //保存文件信息入库
            //入数据库
            MediaFiles mediaFiles = currentProxy.addMediaFileToDb(companyId, uploadFileParamsDto, mergePath, bucket_video, fileMd5);
            if (mediaFiles == null) {
                XuechengPlusException.cast("媒资文件入库出错");
            }
            return RestResponse.success();
        } finally {
            //删除临时文件
            for (File file : chunkFiles) {
                try {
                    file.delete();
                } catch (Exception e) {

                }
            }
            try {
                merge.delete();
            } catch (Exception e) {

            }
        }

    }

    //获取合并后文件的上传路径
    private String getMergeFileFolderPath(String fileMd5, String extName) {
        return fileMd5.substring(0,1) + "/" + fileMd5.substring(1,2) + "/" + fileMd5 + "/" +fileMd5 + extName;
    }

    //下载所有分块文件
    private File[] checkChunkStatus(String fileMd5, int chunkTotal) {
        //获取分块文件夹路径
        String chunkFileFolderPath = getChunkFileFolderPath(fileMd5);
        File[] res = new File[chunkTotal];
        for (int i = 0; i < chunkTotal; i++) {
            //下载分块文件
            String objectName = chunkFileFolderPath + i;
            File chunk = null;
            try {
                chunk = File.createTempFile("chunk", "");
            } catch (IOException e) {
                XuechengPlusException.cast("创建临时文件出错");
            }
            downloadFileFromOSS(chunk, objectName, bucket_video);
            res[i] = chunk;
        }
        return res;
    }

    @Override
    public RestResponse<String> getPlayUrlByMediaId(String mediaId) {
        MediaFiles mediaFiles = mediaFilesMapper.selectById(mediaId);
        if(mediaFiles == null || mediaFiles.getUrl() == null) {
            XuechengPlusException.cast("文件为空或当前文件尚未做转码处理");
        }
        return RestResponse.success(mediaFiles.getUrl());
    }


    @Override
    public void addMediaFileToOss(String absolutePath, String bucket_file, String objectName) {
        //扩展名
        String extension = null;
        if(objectName.indexOf(".")>=0){
            extension = objectName.substring(objectName.lastIndexOf("."));
        }
        //获取扩展名对应的媒体类型
        String contentType = getMimeTypeByExtension(extension);
        try {
            UploadObjectArgs uploadObjectArgs = UploadObjectArgs.builder()
                    .bucket(bucket_video)
                    .filename(absolutePath)
                    .object(objectName)
                    .contentType(contentType)
                    .build();
            minioClient.uploadObject(uploadObjectArgs);
        } catch (Exception e) {
            e.printStackTrace();
            XuechengPlusException.cast("上传文件异常");
        }
    }

    //根据文件后缀获取对应的扩展名
    private String getMimeTypeByExtension(String extension) {
        String contentType = MediaType.APPLICATION_OCTET_STREAM_VALUE;
        if(StringUtils.isNotEmpty(extension)){
            ContentInfo extensionMatch = ContentInfoUtil.findExtensionMatch(extension);
            if(extensionMatch!=null){
                contentType = extensionMatch.getMimeType();
            }
        }
        return contentType;
    }

    //下载文件
    @Override
    public File downloadFileFromOSS(File chunkFile, String chunkFilePath, String bucket_name) {
        InputStream fileInputStream = null;
        OutputStream fileOutputStream = null;
        try {
            fileInputStream = minioClient.getObject(GetObjectArgs.builder()
                    .bucket(bucket_name)
                    .object(chunkFilePath)
                    .build());
            fileOutputStream = Files.newOutputStream(chunkFile.toPath());
            IOUtils.copy(fileInputStream, fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
            XuechengPlusException.cast("下载分块文件异常");
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return chunkFile;
    }
}
