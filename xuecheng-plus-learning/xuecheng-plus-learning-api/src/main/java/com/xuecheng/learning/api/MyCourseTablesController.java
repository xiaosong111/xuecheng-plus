package com.xuecheng.learning.api;

import com.xuecheng.base.exception.XuechengPlusException;
import com.xuecheng.base.model.PageResult;
import com.xuecheng.learning.model.dto.MyCourseTableParams;
import com.xuecheng.learning.model.dto.XcChooseCourseDto;
import com.xuecheng.learning.model.dto.XcCourseTablesDto;
import com.xuecheng.learning.model.po.XcCourseTables;
import com.xuecheng.learning.service.MyCourseTablesService;
import com.xuecheng.learning.util.SecurityUtil;
import com.xuecheng.learning.util.SecurityUtil.XcUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Mr.M
 * @version 1.0
 * @description 我的课程表接口
 * @date 2022/10/25 9:40
 */

@Api(value = "我的课程表接口", tags = "我的课程表接口")
@Slf4j
@RestController
public class MyCourseTablesController {

    @Autowired
    private MyCourseTablesService myCourseTablesService;

    @ApiOperation("添加选课")
    @PostMapping("/choosecourse/{courseId}")
    public XcChooseCourseDto addChooseCourse(@PathVariable("courseId") Long courseId) {
        XcUser user = SecurityUtil.getUser();
        if(user == null) {
            XuechengPlusException.cast("请登录后在继续选课");
        }
        String userId = user.getId();
        return myCourseTablesService.addChooseCourse(userId,courseId);
    }

    @ApiOperation("查询学习资格")
    @PostMapping("/choosecourse/learnstatus/{courseId}")
    public XcCourseTablesDto getLearnstatus(@PathVariable("courseId") Long courseId) {
        XcUser user = SecurityUtil.getUser();
        if(user == null) {
            XuechengPlusException.cast("请登录后在继续选课");
        }
        XcCourseTablesDto xcCourseTablesDto = myCourseTablesService.getLeanringStatus(user.getId(), courseId);
        return xcCourseTablesDto;

    }

    @ApiOperation("我的课程表")
    @GetMapping("/mycoursetable")
    public PageResult<XcCourseTables> mycoursetable(MyCourseTableParams params) {
        ////TODO 方法是否可用待测试  目前未测试
        XcUser user = SecurityUtil.getUser();
        if(!user.getId().equals(params.getUserId())) {
            XuechengPlusException.cast("权限不足");
        }
        return myCourseTablesService.mycoursetable(params);
    }

}
