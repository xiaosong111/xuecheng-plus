package com.xuecheng.learning.service;

import com.xuecheng.base.model.PageResult;
import com.xuecheng.content.model.po.CoursePublish;
import com.xuecheng.learning.model.dto.MyCourseTableParams;
import com.xuecheng.learning.model.dto.XcChooseCourseDto;
import com.xuecheng.learning.model.dto.XcCourseTablesDto;
import com.xuecheng.learning.model.po.XcChooseCourse;
import com.xuecheng.learning.model.po.XcCourseTables;

/**
 * @Author QG
 * @Date 2023/3/23 17:31
 * @description 我的课程表接口
 **/
public interface MyCourseTablesService {


    /**
     * 添加选课
     * @param userId
     * @param courseId
     * @return
     */
    XcChooseCourseDto addChooseCourse(String userId, Long courseId);

    XcChooseCourse addFreeCoruse(String userId, CoursePublish coursepublish);

    XcChooseCourse addChargeCoruse(String userId, CoursePublish coursepublish);

    /**
     * 向我的课程表插入记录
     * @param xcChooseCourse
     * @return
     */
    XcCourseTables addCourseTabls(XcChooseCourse xcChooseCourse);


    /**
     * 获取学习资格
     * @param userId
     * @param courseId
     * @return XcCourseTablesDto 学习资格状态 [{"code":"702001","desc":"正常学习"},
     * {"code":"702002","desc":"没有选课或选课后没有支付"},{"code":"702003","desc":"已过期需要申请续期或重新支付"}]
     */
    XcCourseTablesDto getLeanringStatus(String userId,Long courseId);

    /**
     * 根据条件查询我的课程表
     * @param params
     * @return
     */
    PageResult<XcCourseTables> mycoursetable(MyCourseTableParams params);
}
